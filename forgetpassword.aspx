﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="forgetpassword.aspx.cs" Inherits="forgetpassword" UnobtrusiveValidationMode="None" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Log in</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="bootstrap/fonts/font-awesome.min.css" rel="stylesheet" />
   <link href="Styles/LandingPage.css" rel="stylesheet" />
    <link href="Styles/Google-Style-Login.css" rel="stylesheet" />
    <link href="Styles/styles.css" rel="stylesheet" />
  <link href="fonts/font-awesome.min.css" rel="stylesheet" />
    <script src="Scripts/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="Scripts/alertmessages.js"></script>
     <style type="text/css">

        .messagealert {
            width: 30%;
            position: fixed;
             top:0px;
            z-index: 100000;
            padding: 0;
            font-size: 15px;
            margin-right: 50%
        }
    </style>
   
</head>
    <%--<form id="form1" runat="server">
    <div>
    
    </div>
    </form>--%>
    <body>
    <div class="login-card"><asp:Image runat="server"  CssClass="profile-img-card" ImageUrl="~/img/forgetpassword.jpg"/>
        <p class="profile-name-card"> </p>

        <form runat="server" class="form-signin"><span class="reauth-email"> </span>

            <div class="messagealert" id="alert_container">
            </div>
            <asp:TextBox runat="server" ID="txtemail" placeholder="Enter Email" class="form-control" required="true" ></asp:TextBox>
            <asp:RequiredFieldValidator ErrorMessage="Required" Display="Dynamic" ForeColor="Red"
                     ControlToValidate="txtemail" runat="server" />
                <asp:RegularExpressionValidator runat="server" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                ControlToValidate="txtEmail" ForeColor="Red" ErrorMessage="Invalid email address." />
            <br />
                <asp:Label ID="lblMessage" runat="server"  ForeColor="Blue"/>
            <br />
           <asp:Button runat="server" ID="btnrecoveremail" class="btn btn-success btn-block btn-lg btn-signin" Text="Recover Email"  OnClick="btnrecoveremail_Click"/>
            <a href="login.aspx" class="alert-link">Log In</a>
            <%--<button class="btn btn-primary btn-block btn-lg btn-signin" type="submit">Sign in</button>--%>
        </form>

    </div>
        <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h5>Powered By Sekat Technologies © 2017</h5></div>
                <div class="col-sm-6 social-icons"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter-square"></i></a><a href="#"><i class="fa fa-instagram"></i></a></div>
            </div>
        </div>
    </footer>
    <script src="Scripts/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="Scripts/alertmessages.js"></script>
</body>
</html>
