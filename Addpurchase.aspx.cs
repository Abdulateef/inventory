﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Addpurchase : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!this.IsPostBack)
        {
            populateItemDropDownList();
            populateItemDropDownList2();
            return_giftDropDownList();
        }
    }

    protected void ShowMessage(string Message, EnumMessages.MessageType type)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
    }
    public void populateItemDropDownList()
    {
        string erromessage = "";
        try
        {
            using(OdbcConnection con = DBConnection.getconnectionString(erromessage))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(DBqueries.updatedropdwonlistquery, con))
                {
                    drpitemslist.DataSource = com.ExecuteReader();
                    drpitemslist.DataTextField = "id";
                    drpitemslist.DataTextField = "name";
                    drpitemslist.DataBind();
                    con.Close();
                }
            }
            drpitemslist.Items.Insert(0, new ListItem("--Select Product--", "0"));
        }
        catch(Exception ex)
        {
            erromessage = "An Error has Occured";
        }
    }

    public void return_giftDropDownList()
    {
        string erromessage = "";
        try
        {
            using(OdbcConnection con = DBConnection.getconnectionString(erromessage))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(DBqueries.updatedropdwonlistquery, con))
                {
                    drplistrgitems.DataSource = com.ExecuteReader();
                    drplistrgitems.DataTextField = "id";
                    drplistrgitems.DataTextField = "name";
                    drplistrgitems.DataBind();
                    con.Close();
                }
            }
            drplistrgitems.Items.Insert(0, new ListItem("--Select Product--", "0"));
        }
        catch(Exception ex)
        {
            erromessage = "An Error has Occured";
        }
    }

    public void populateItemDropDownList2()
    {
        string erromessage = "";
        try
        {
            using(OdbcConnection con = DBConnection.getconnectionString(erromessage))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(DBqueries.updatedropdwonlistquery2, con))
                {
                    ddlitemremove.DataSource = com.ExecuteReader();
                    ddlitemremove.DataTextField = "id";
                    ddlitemremove.DataTextField = "name";
                    ddlitemremove.DataBind();
                    con.Close();
                }
            }
            ddlitemremove.Items.Insert(0, new ListItem("--Select Product--", "0"));
        }
        catch(Exception ex)
        {
            erromessage = "An Error has Occured";
        }
    }


    public void updatecodetextboxreturnitems()
    {
        string erroMessage = "";
        string code = "";
        string category = "";
        try
        {
            string str = "Select id, code,category,  name, s_price,  c_price, stock, qtysold from items where" + " name =?";
            using(OdbcConnection con = DBConnection.getconnectionString(erroMessage))
            {
                con.Open();

                using(OdbcCommand vercommand = new OdbcCommand(str, con))
                {
                    vercommand.Parameters.AddWithValue("?name", drplistrgitems.SelectedItem.Value);
                    OdbcDataReader reader = vercommand.ExecuteReader();

                    while(reader.Read())
                    {
                        code = reader["code"].ToString();
                        category = reader["category"].ToString();
                    }

                    //update code textbox
                    txtcodergitem.Text = code;
                    txtrgcategory.Text = category;
                }
            }
        }
        catch(Exception)
        {

            throw;
        }
    }


    public void updatecodetextbox()
    {
        string erroMessage = "";
        string code = "";
        string category = "";
        try
        {
            string str = "Select id, code,category,  name, s_price,  c_price, stock, qtysold from items where" + " name =?";
            using(OdbcConnection con = DBConnection.getconnectionString(erroMessage))
            {
                con.Open();

                using(OdbcCommand vercommand = new OdbcCommand(str, con))
                {
                    vercommand.Parameters.AddWithValue("?name", drpitemslist.SelectedItem.Value);
                    OdbcDataReader reader = vercommand.ExecuteReader();

                    while(reader.Read())
                    {
                        code = reader["code"].ToString();
                        category = reader["category"].ToString();
                    }

                    //update code textbox
                    txtcode.Text = code;
                    txtcategory.Text = category;
                }
            }
        }
        catch(Exception)
        {

            throw;
        }
    }

    public void updateCodeTextBoxRemove()
    {
        string erroMessage = "";
        string code = "";
        try
        {
            string str = "Select id, code from purchase where" + " name =?";
            using(OdbcConnection con = DBConnection.getconnectionString(erroMessage))
            {
                con.Open();

                using(OdbcCommand vercommand = new OdbcCommand(str, con))
                {
                    vercommand.Parameters.AddWithValue("?name", ddlitemremove.SelectedItem.Value);
                    OdbcDataReader reader = vercommand.ExecuteReader();

                    while(reader.Read())
                    {
                        code = reader["code"].ToString();
                    }

                    //update code textbox
                    txtcoderemove.Text = code.ToString();
                }
            }
        }
        catch(Exception)
        {

            throw;
        }
    }

    public void insertintoreturnitem()
    {
        string erroMessage = "";
        string code = "";
        string name = "";
        string category = "";
        string costPrice = "";
        string stock = "";
        int stockvalue = 0;
        try
        {
            string str = "Select id, code,category,  name, s_price,  c_price, stock, qtysold from items where" + " name =? AND code=?";
            using(OdbcConnection con = DBConnection.getconnectionString(erroMessage))
            {
                con.Open();

                using(OdbcCommand vercommand = new OdbcCommand(str, con))
                {
                    vercommand.Parameters.AddWithValue("?name", drplistrgitems.SelectedItem.Value);
                    vercommand.Parameters.AddWithValue("?name", txtcodergitem.Text.ToString());
                    OdbcDataReader reader = vercommand.ExecuteReader();

                    while(reader.Read())
                    {
                        code = reader["code"].ToString();
                        name = reader["name"].ToString();
                        category = reader["category"].ToString();
                        costPrice = reader["c_price"].ToString();
                        stock = reader["stock"].ToString();
                    }

                    //upadte stock amount after adding new return items
                    int rowaffected = 0;
                    stockvalue = Convert.ToInt32(stock) - Convert.ToInt32(txtQauntity.Text);
                    string updatestock = "UPDATE items SET stock=?  where" + " name =? AND code=?";
                    using(OdbcCommand comm = new OdbcCommand(updatestock, con))
                    {
                        comm.Parameters.AddWithValue("?stock", stockvalue);
                        comm.Parameters.AddWithValue("?name", drpitemslist.SelectedItem.Value);
                        comm.Parameters.AddWithValue("?code", txtcode.Text.ToString());
                        rowaffected = comm.ExecuteNonQuery();
                        if(rowaffected <= 0)
                        {
                            ShowMessage("Oops an error has occured", EnumMessages.MessageType.Error);
                        }
                    }

                    //inserting return item into the returnitemtable
                    try
                    {
                        int rowcount = 0;
                        using(OdbcCommand com = new OdbcCommand(DBqueries.insertintoreturngoods, con))
                        {
                            com.Parameters.AddWithValue("?name", name);
                            com.Parameters.AddWithValue("?category", category);
                            com.Parameters.AddWithValue("?narration", txtnarration.Text.ToString());
                            com.Parameters.AddWithValue("?quantity", txtQauntity);
                            com.Parameters.AddWithValue("?total", Convert.ToDouble(txtrgtotalamount.Text.ToString()));
                            com.Parameters.AddWithValue("?quantity", Convert.ToInt32(txtqauntityrgitem.Text.ToString()));
                            com.Parameters.AddWithValue("?code", code.ToString());
                            com.Parameters.AddWithValue("?sellinprice", Convert.ToDouble(txtrgcost.Text.ToString()));
                            com.Parameters.AddWithValue("?user", Globalvariables.username);
                            rowcount = com.ExecuteNonQuery();

                            if(rowcount > 0)
                            {
                                ShowMessage("New record has been Upadated", EnumMessages.MessageType.Success);
                            }
                        }

                    }
                    catch(Exception ex)
                    {

                        ShowMessage("Oops an error occured while updating records", EnumMessages.MessageType.Error);
                    }

                }
            }
        }
        catch(Exception)
        {

            ShowMessage("Oops an error has occured", EnumMessages.MessageType.Error);
        }
    }

    public void insertintopurchase()
    {
        string erroMessage = "";
        string code = "";
        string name = "";
        string category = "";
        string costPrice = "";
        string stock = "";
        int stockvalue = 0;
        try
        {
            string str = "Select id, code,category,  name, s_price,  c_price, stock, qtysold from items where" + " name =? AND code=?";
            using(OdbcConnection con = DBConnection.getconnectionString(erroMessage))
            {
                con.Open();

                using(OdbcCommand vercommand = new OdbcCommand(str, con))
                {
                    vercommand.Parameters.AddWithValue("?name", drpitemslist.SelectedItem.Value);
                    vercommand.Parameters.AddWithValue("?name", txtcode.Text.ToString());
                    OdbcDataReader reader = vercommand.ExecuteReader();

                    while(reader.Read())
                    {
                        code = reader["code"].ToString();
                        name = reader["name"].ToString();
                        category = reader["category"].ToString();
                        costPrice = reader["c_price"].ToString();
                        stock = reader["stock"].ToString();
                    }

                    //upadte stock amount after adding new purchase items
                    int rowaffected = 0;
                    stockvalue = Convert.ToInt32(txtQauntity.Text) + Convert.ToInt32(stock);
                    string updatestock = "UPDATE items SET stock=?  where" + " name =? AND code=?";
                    using(OdbcCommand comm = new OdbcCommand(updatestock, con))
                    {
                        comm.Parameters.AddWithValue("?stock", stockvalue);
                        comm.Parameters.AddWithValue("?name", drpitemslist.SelectedItem.Value);
                        comm.Parameters.AddWithValue("?code", txtcode.Text.ToString());
                        rowaffected = comm.ExecuteNonQuery();
                        if(rowaffected <=0 )
                        {
                            ShowMessage("Oops an error has occured", EnumMessages.MessageType.Error);
                        }
                    }

                    //inserting new purchased item into the records
                    try
                    {
                        int rowcount = 0;
                        using(OdbcCommand com = new OdbcCommand(DBqueries.insertintopurchase, con))
                        {
                            com.Parameters.AddWithValue("?name", name);
                            com.Parameters.AddWithValue("?code", code);
                            com.Parameters.AddWithValue("?category", category);
                            com.Parameters.AddWithValue("?quantity", txtQauntity);
                            com.Parameters.AddWithValue("?costprice", costPrice);
                            com.Parameters.AddWithValue("?user", Globalvariables.username);
                            com.Parameters.AddWithValue("?narration", txtnarration.Text.ToString());
                            com.Parameters.AddWithValue("?suppliername", txtsuppliername.Text.ToString());
                            com.Parameters.AddWithValue("?invoicenumber", txtcategory.Text.ToString());
                            com.Parameters.AddWithValue("?sellinprice", txtsellingprice.Text.ToString());
                            com.Parameters.AddWithValue("?pack", drppackunit.SelectedItem.Text);
                            rowcount = com.ExecuteNonQuery();

                            if(rowcount > 0)
                            {
                                //lblerror.Text = "User has been Created";
                                //lblerror.ForeColor = System.Drawing.Color.Blue;
                                ShowMessage("New Purchasse has been Upadated", EnumMessages.MessageType.Success);

                            }
                        }

                    }
                    catch(Exception ex)
                    {

                        ShowMessage("Oops an error occured", EnumMessages.MessageType.Error);
                    }

                }
            }
        }
        catch(Exception)
        {

            ShowMessage("Oops an error has occured", EnumMessages.MessageType.Error);
        }
    }

    public void removeFromPurchase()
    {
        string erroMessage = "";
        string code = "";
        string name = "";
        string category = "";
        string costPrice = "";
        string stock = "";
        int stockvalue = 0;
        try
        {
            string str = "Select id, code,category,  name, s_price,  c_price, stock, qtysold from items where" + " name =? AND code=?";
            using(OdbcConnection con = DBConnection.getconnectionString(erroMessage))
            {
                con.Open();

                using(OdbcCommand vercommand = new OdbcCommand(str, con))
                {
                    vercommand.Parameters.AddWithValue("?name", ddlitemremove.SelectedItem.Value);
                    vercommand.Parameters.AddWithValue("?name", txtcoderemove.Text.ToString());
                    OdbcDataReader reader = vercommand.ExecuteReader();

                    while(reader.Read())
                    {
                        code = reader["code"].ToString();
                        name = reader["name"].ToString();
                        category = reader["category"].ToString();
                        costPrice = reader["c_price"].ToString();
                        stock = reader["stock"].ToString();
                    }

                    //upadte stock amount after adding new purchase items
                    int rowaffected = 0;
                    stockvalue = Convert.ToInt32(stock) - Convert.ToInt32(txtquantityremove.Text);
                    string updatestock = "UPDATE items SET stock=?  where" + " name =? AND code=?";
                    using(OdbcCommand comm = new OdbcCommand(updatestock, con))
                    {
                        comm.Parameters.AddWithValue("?name", ddlitemremove.SelectedItem.Value);
                        comm.Parameters.AddWithValue("?name", txtcoderemove.Text.ToString());
                        comm.Parameters.AddWithValue("?stock", stockvalue);
                        try
                        {
                            rowaffected = comm.ExecuteNonQuery();
                        }
                        catch(Exception ex)
                        {
                            ShowMessage("Oops an error has occured", EnumMessages.MessageType.Error);
                        }


                    }

                    //deleting new purchased item into the records
                    try
                    {
                        int rowcount = 0;
                        using(OdbcCommand com = new OdbcCommand(DBqueries.deleteFromPurchase, con))
                        {
                            com.Parameters.AddWithValue("?name", name);
                            com.Parameters.AddWithValue("?code", code);
                            com.Parameters.AddWithValue("?category", category);
                            com.Parameters.AddWithValue("?quantity", txtQauntity);
                            com.Parameters.AddWithValue("?costprice", costPrice);
                            com.Parameters.AddWithValue("?invoicenumber", txtinvoiceremove.Text.ToString());
                            rowcount = com.ExecuteNonQuery();

                            if(rowcount > 0)
                            {
                                ShowMessage("Record has been Removed", EnumMessages.MessageType.Success);
                            }
                        }

                    }
                    catch(Exception ex)
                    {

                        ShowMessage("Oops an error occured", EnumMessages.MessageType.Error);
                    }

                }
            }
        }
        catch(Exception)
        {

            ShowMessage("Oops an error has occured", EnumMessages.MessageType.Error);
        }
    }
    protected void btnaddpurchase_Click(object sender, EventArgs e)
    {
        insertintopurchase();
    }
    protected void btncancelladnewitem_Click(object sender, EventArgs e)
    {
        txtcode.Text = "";
        drpitemslist.SelectedItem.Text = "";
        txtQauntity.Text = "";
    }
    protected void drpitemslist_SelectedIndexChanged(object sender, EventArgs e)
    {
        updatecodetextbox();
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        txtcoderemove.Text = "";
        txtquantityremove.Text = "";
        ddlitemremove.Text = "";
    }
    protected void btnremove_Click(object sender, EventArgs e)
    {
        removeFromPurchase();
    }
    protected void ddlitemremove_SelectedIndexChanged(object sender, EventArgs e)
    {
        updateCodeTextBoxRemove();
    }
    protected void drppackunit_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btncancelpurchase_Click(object sender, EventArgs e)
    {

    }
    protected void drplistrgitems_SelectedIndexChanged(object sender, EventArgs e)
    {
        updatecodetextboxreturnitems();
    }
    protected void btncancelreturnitem_Click(object sender, EventArgs e)
    {
        txtrgcategory.Text = "";
        txtrgcost.Text = "";
        txtrgdate.Text = "";
        txtqauntityrgitem.Text = "";
        txtrgnarration.Text = "";
        txtrgtotalamount.Text = "";

    }
}