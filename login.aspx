﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" UnobtrusiveValidationMode="None" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Log in</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="bootstrap/fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="Styles/Google-Style-Login.css" rel="stylesheet" />
    <link href="Styles/LandingPage.css" rel="stylesheet" />
    <link href="Styles/styles.css" rel="stylesheet" />
    <link href="fonts/font-awesome.min.css" rel="stylesheet" />
    <script src="Scripts/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="Scripts/alertmessages.js"></script>

    <style type="text/css">

        .messagealert {
            width: 100%;
            position: fixed;
             top:0px;
            z-index: 100000;
            padding: 0;
            font-size: 15px;
             margin-right: 50%
        }
    </style>
</head>
    <%--<form id="form1" runat="server">
    <div>
    
    </div>
    </form>--%>
    <body>

    <div class="login-card"><asp:Image runat="server"  CssClass="profile-img-card" ImageUrl="~/img/logimg.jpg"/>
        <p class="profile-name-card"> </p>
        <form runat="server" class="form-signin"><span class="reauth-email"> </span>
            <div class="messagealert" id="alert_container">
            </div>
            <asp:TextBox runat="server" ID="txtusername" placeholder="User Name" class="form-control" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" id="reqName" controltovalidate="txtusername" errormessage="Please enter your name!" />
            <asp:TextBox runat="server" ID="txtpassword" placeholder="Password" class="form-control" ToolTip="Enter password" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" id="Reqpassword" controltovalidate="txtpassword" errormessage="Please enter your password!" />
            <div class="checkbox">
                <div class="checkbox">
                    <label>
                        <asp:CheckBox runat="server"  Text="Remember me"/>
                     </label>
                </div>
            </div>
            <asp:Button runat="server" class="btn btn-primary btn-block btn-lg btn-signin" Text="Sign in" ID="btnlogin" OnClick="btnlogin_Click"/>
           <%-- <button class="btn btn-primary btn-block btn-lg btn-signin" type="submit">Sign in</button>--%>
        </form>
        <a href="forgetpassword.aspx" class="forgot-password">Forgot your password?</a>
        <br />
        <a href="Default.aspx" class="btn btn btn-success">Home</a>

    </div>
        <asp:Label runat="server" ID="lblerror"></asp:Label>
        <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h5>Powered By Sekat Technologies © 2017</h5></div>
                <div class="col-sm-6 social-icons"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter-square"></i></a><a href="#"><i class="fa fa-instagram"></i></a></div>
            </div>
        </div>
    </footer>
    <script src="Scripts/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
