﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Odbc;
using System.Data;

public partial class menu : System.Web.UI.Page
{
    Int64 stockview;
    Int64 balnewd;
    Int64 sum = 0;
    Int64 mycheckstock = 0;
    Int64 ded = 0;
    Int64 dedview = 0;
    Int64 Stock2 = 0;
    Int64 balstock;
    Int64 bals = 0;
    Int64 subb = 0;

    protected void Page_Load(object sender, EventArgs e)
    {


        lblerror.Visible = false;
        if(!this.IsPostBack)
        {
            populateItemDropDownList();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[7] { new DataColumn("ProductName"), new DataColumn("Category"), new DataColumn("Code"), new DataColumn("Stock"), new DataColumn("Quantity"), new DataColumn("Price"), new DataColumn("Total") });
            ViewState["dt"] = dt;
            //this.BindGrid();
            deleteReceipt();
        }
    }

    // method to display error
    protected void ShowMessage(string Message, EnumMessages.MessageType type)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
    }
    //method to bind data 
    protected void BindGrid()
    {
        GridProduct.DataSource = ViewState["dt"] as DataTable;
        GridProduct.DataBind();
    }


    //method to populate the dropdown list on page load
    public void populateItemDropDownList()
    {
        lblerror.Text = "";
        try
        {
            using(OdbcConnection con = DBConnection.getconnectionString(lblerror.Text))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(DBqueries.updatedropdwonlistquery, con))
                {
                    drpitemslist.DataSource = com.ExecuteReader();
                    drpitemslist.DataTextField = "id";
                    drpitemslist.DataTextField = "name";
                    drpitemslist.DataBind();
                    con.Close();
                }
            }
            drpitemslist.Items.Insert(0, new ListItem("--Select Product--", "0"));
        }
        catch(Exception ex)
        {

            ShowMessage("An Error has Occured", EnumMessages.MessageType.Error);
        }
    }



    public void populategridview()
    {
        int currentstockvalue = 0;
        int currentstock = 0;
        string value;
        currentstock = Convert.ToInt32(txtStock.Text.ToString());
        if(currentstock == 0)
        {
            ShowMessage("OUT OF STOCK!", EnumMessages.MessageType.Info);
        }
        try
        {
            string c_price = "";
            string code = "";
            string stock = "";
            string category = "";
            string name = "";

            GridProduct.Visible = true;
            string s = drpitemslist.SelectedValue.ToString();
            OdbcConnection coon = DBConnection.getconnectionString(lblerror.Text);
            coon.Open();

            string str = "Select id, code,category,  name, s_price,  c_price, stock, qtysold from items where name='" + drpitemslist.SelectedItem.Value + "'";
            OdbcCommand command = new OdbcCommand(str, coon);
            OdbcDataReader reader = command.ExecuteReader();

            while(reader.Read())
            {
                code = reader["code"].ToString();
                stock = reader["stock"].ToString();
                c_price = reader["c_price"].ToString();
                category = reader["category"].ToString();
                name = reader["name"].ToString();
                Session["stockverify"] = stock;
                //txtStock.Text = stock;

            }
            reader.Close();
            Int64 totalprice;
            totalprice = Int64.Parse(c_price) * Int64.Parse(txtitemquantity.Text);
            DataTable dt = (DataTable)ViewState["dt"];
            txtitemquantity.Text = txtitemquantity.Text;
            ViewState["dt"] = dt;

            //reducing the tempoary store on Add button click

            currentstockvalue = currentstock - Convert.ToInt32(txtitemquantity.Text);
            if(currentstockvalue <= 0)
            {
                currentstockvalue = 0;
            }
            value = currentstockvalue.ToString();
            txtStock.Text = value;
            dt.Rows.Add(name, category, code, txtStock.Text, txtitemquantity.Text, c_price, totalprice);
            this.BindGrid();

            //Calculate Sum and display in Footer Row
            int sum = 0;
            for(int i = 0; i < GridProduct.Rows.Count; i++)
            {
                sum += int.Parse(GridProduct.Rows[i].Cells[7].Text);
            }

            int labeltotal = sum;
            GridProduct.FooterRow.Cells[6].Text = "Total";
            GridProduct.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Right;
            GridProduct.FooterRow.Cells[7].Text = labeltotal.ToString("N2");
            txtfinalbill.Text = labeltotal.ToString("D");
            coon.Close();
        }
        catch(Exception ex)
        {

            ShowMessage("Oops an error has Occured", EnumMessages.MessageType.Error);

        }
    }

    public void updatestocktextbox()
    {
        string idverify = "";
        string name = "";
        string stockverify = "";
        string code = "";
        txtStock.Text = "";
        try
        {
            string str = "Select id, code,category,  name, s_price,  c_price, stock, qtysold from items where" + " name =?";
            using(OdbcConnection con = DBConnection.getconnectionString(lblerror.Text))
            {
                con.Open();

                using(OdbcCommand vercommand = new OdbcCommand(str, con))
                {
                    vercommand.Parameters.AddWithValue("?name", drpitemslist.SelectedItem.Value);
                    OdbcDataReader reader = vercommand.ExecuteReader();

                    while(reader.Read())
                    {
                        idverify = reader["id"].ToString();
                        name = reader["name"].ToString();
                        code = reader["code"].ToString();
                        stockverify = reader["stock"].ToString();
                    }
                    if(stockverify == "0")
                    {
                        txtStock.Text = Convert.ToInt32(0).ToString();
                    }
                    else
                    {
                        txtStock.Text = stockverify.ToString();
                    }
                    txtStock.Text = stockverify;
                    txtcode.Text = code;
                }
            }
        }
        catch(Exception)
        {

            throw;
        }
    }


    public void savePurchases()
    {
        //checking the transaction type
        string trasanctiontype = "";
        if(rdbCash.Checked)
        {
            trasanctiontype = "Cash";
        }
        else if(rdbCheck.Checked)
        {
            trasanctiontype = "Check";
        }
        //global variable to pass information around the application and also to the parameter used in the rdlc report
        Globalvariables.purchasetype = trasanctiontype;
        Globalvariables.customername = txtname.Text.ToString();
        Globalvariables.customeraddress = txtContact.Text.ToString();
        try
        {
            using(OdbcConnection conn = DBConnection.getconnectionString(lblerror.Text))
            {
                conn.Open();

                using(OdbcCommand comm = new OdbcCommand(DBqueries.savepurchasequries, conn))
                {
                    decimal total = Convert.ToDecimal(txtfinalbill.Text);
                    decimal fianlbill =  Convert.ToDecimal(txtfinalbill.Text);
                    comm.Parameters.AddWithValue("?type", trasanctiontype);
                    comm.Parameters.AddWithValue("?name", txtname.Text);
                    comm.Parameters.AddWithValue("?contact", txtContact.Text);
                    comm.Parameters.AddWithValue("?total", txtfinalbill.Text);
                    comm.Parameters.AddWithValue("?final", txtfinalbill.Text);
                    comm.ExecuteNonQuery();
                }

                foreach(GridViewRow row in GridProduct.Rows)
                {
                    Int64 cde;
                    Int64 qty;
                    Int64 stock2;
                    Int64 newstock;
                    string stock;
                    //insertind sales into itemsoldoffline table
                    var cmmd = conn.CreateCommand();
                    cmmd.CommandText = "INSERT INTO itemsoldoffline(code,product,quantity,user ) VALUES(?,?,?,?)";
                    {
                        cmmd.Parameters.AddWithValue("?code", row.Cells[1].Text);
                        cmmd.Parameters.AddWithValue("?product", row.Cells[2].Text);
                        cmmd.Parameters.AddWithValue("?quantity", row.Cells[6].Text);
                        //cmmd.Parameters.AddWithValue("?user", Session["user"].ToString());
                        cmmd.Parameters.AddWithValue("?user", "abdulateef");
                        cmmd.ExecuteNonQuery();
                    }
                    cde = Int64.Parse((row.Cells[1]).Text);
                    //qty = Int64.Parse(rom.Cells[5].Text);
                    // getting the current stock a particular item code
                    string str = "Select id, code,category,  name, s_price,  c_price, stock, qtysold from items where code='" + cde + "'";
                    using(OdbcCommand sqcommand = new OdbcCommand(str, conn))
                    {
                        OdbcDataReader reader = sqcommand.ExecuteReader();
                        while(reader.Read())
                        {
                            stock = reader["stock"].ToString();
                            string updatestck;
                            newstock = Int64.Parse(stock) - Int64.Parse((row.Cells[6]).Text);
                            updatestck = newstock.ToString();
                            string mquery = "update items set stock='" + updatestck + "' where code='" + cde + "';";
                            using(OdbcCommand command = new OdbcCommand(mquery, conn))
                            {
                                command.ExecuteNonQuery();
                            }
                        }

                    }

                    // inserting records into rptsoldoffline
                    using(OdbcCommand comm = new OdbcCommand(DBqueries.insertrptitemsoldofflinequery, conn))
                    {
                        comm.Parameters.AddWithValue("?name", "Abdulateef");
                        comm.Parameters.AddWithValue("?type", trasanctiontype.ToString());
                        comm.Parameters.AddWithValue("?contact", txtContact.Text);
                        comm.Parameters.AddWithValue("?code", row.Cells[1].Text);
                        comm.Parameters.AddWithValue("?product", row.Cells[2].Text);
                        comm.Parameters.AddWithValue("?quantity", row.Cells[6].Text);
                        comm.Parameters.AddWithValue("?price", row.Cells[4].Text);
                        comm.Parameters.AddWithValue("?total", row.Cells[7].Text);
                        //comm.Parameters.AddWithValue("@totalamount", lblFinalBill.Text);
                        comm.Parameters.AddWithValue("?final", row.Cells[7].Text);
                        comm.ExecuteNonQuery();
                    }

                    // inserting records into receipt
                    using(OdbcCommand comm = new OdbcCommand(DBqueries.insertintoreceipt, conn))
                    {
                        comm.Parameters.AddWithValue("?Name", row.Cells[2].Text);
                        comm.Parameters.AddWithValue("?Price", row.Cells[4].Text);
                        comm.Parameters.AddWithValue("?Quantity", row.Cells[6].Text);
                        comm.Parameters.AddWithValue("?Total", row.Cells[7].Text);
                        comm.ExecuteNonQuery();
                    }
                }
            }
        }
        catch(Exception ex)
        {
            ShowMessage("Oopps an has occured", EnumMessages.MessageType.Error);
        }

    }

    public void deleteReceipt()
    {
        string errormessage = " ";
        try
        {
             using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
        {
            con.Open();
            using(OdbcCommand com = new OdbcCommand(DBqueries.deletereceipt, con))
            {
                com.ExecuteNonQuery();
            }
        }
        }
        catch(Exception)
        {
            
            throw;
        }
    }


    public void deletetemporarysale()
    {
        string errormessage = " ";
        try
        {
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(DBqueries.deletetemsalesitems,con))
                {
                    com.ExecuteNonQuery();
                }
            }
        }
        catch(Exception)
        {

            throw;
        }
    }
    public void checkstock()
    {
        string totalquantity = "";
        string code = "";
        string stockvaluee = "";
        int stock = 0;
        try
        {
            //query to get the code of the selected product name 
            string str = "Select code, stock from items where" + " name =?";
            using(OdbcConnection con = DBConnection.getconnectionString(lblerror.Text))
            {
                con.Open();

                using(OdbcCommand vercommand = new OdbcCommand(str, con))
                {
                    vercommand.Parameters.AddWithValue("?name", drpitemslist.SelectedItem.Value);
                    OdbcDataReader reader = vercommand.ExecuteReader();

                    while(reader.Read())
                    {

                        code = reader["code"].ToString();
                        stockvaluee = reader["stock"].ToString();
                    }
                }
                string strsum = "Select SUM(quantty) AS total from temsalesitems WHERE productname=? AND code=?";

                using(OdbcCommand sqcommand = new OdbcCommand(strsum, con))
                {
                    sqcommand.Parameters.AddWithValue("?productname", drpitemslist.SelectedItem.Value);
                    sqcommand.Parameters.AddWithValue("?code", code);
                    OdbcDataReader reader = sqcommand.ExecuteReader();
                    while(reader.Read())
                    {
                        totalquantity = reader["total"].ToString();
                    }

                    stock = Convert.ToInt32(stockvaluee) - Convert.ToInt32(totalquantity);

                    if(stock <= 0)
                    {
                        txtStock.Text = Convert.ToInt32(0).ToString();
                    }
                    else
                    {
                        txtStock.Text = stock.ToString();
                    }
                    txtcode.Text = code.ToString();
                }
            }
        }
        catch(Exception ex)
        {
            ShowMessage(ex.Message, EnumMessages.MessageType.Error);
        }

    }

    //Calculate Sum and display in Footer Row and final bil textbox
    public void calculateSum()
    {

        int sum = 0;
        for(int i = 0; i < GridProduct.Rows.Count; i++)
        {
            sum += int.Parse(GridProduct.Rows[i].Cells[7].Text);
        }

        int labeltotal = sum;
        GridProduct.FooterRow.Cells[6].Text = "Total";
        GridProduct.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Right;
        GridProduct.FooterRow.Cells[7].Text = labeltotal.ToString("N2");
        txtfinalbill.Text = labeltotal.ToString("D");
    }

    //store the last  sales on every add button click  temporarly incase page was refreshed
    public void addsalestotemparytable()
    {

        if(GridProduct.Rows.Count == 0)
        {

            ShowMessage("No record yet", EnumMessages.MessageType.Info);
        }
        else
        {
            GridViewRow rowtwo = GridProduct.Rows[GridProduct.Rows.Count - 1];

            //string index = rowtwo.Cells.ToString();

            using(OdbcConnection conn = DBConnection.getconnectionString(lblerror.Text))
            {
                int index = 0;
                int rownumber = 0;
                conn.Open();
                using(OdbcCommand comm = new OdbcCommand(DBqueries.temsalesitemsquery, conn))
                {
                    index = rowtwo.RowIndex;
                    rownumber = index + 1;
                    comm.Parameters.AddWithValue("?rownumber", rownumber);
                    comm.Parameters.AddWithValue("?code", rowtwo.Cells[1].Text);
                    comm.Parameters.AddWithValue("?productname", rowtwo.Cells[2].Text);
                    comm.Parameters.AddWithValue("?category", rowtwo.Cells[3].Text.ToString());
                    comm.Parameters.AddWithValue("?price", Convert.ToInt32(rowtwo.Cells[4].Text.ToString()));
                    comm.Parameters.AddWithValue("?stock", Convert.ToInt32(rowtwo.Cells[5].Text.ToString()));
                    comm.Parameters.AddWithValue("?quantty", Convert.ToInt32(rowtwo.Cells[6].Text.ToString()));
                    comm.Parameters.AddWithValue("?total", Convert.ToInt32(rowtwo.Cells[7].Text.ToString()));
                    comm.ExecuteNonQuery();
                }
            }
        }
    }

    protected void drpitemslist_SelectedIndexChanged(object sender, EventArgs e)
    {
        string name = "";
        string str = "SELECT DISTINCT productname AS name FROM  temsalesitems WHERE productname=?";
        using(OdbcConnection con = DBConnection.getconnectionString(lblerror.Text))
        {
            con.Open();

            using(OdbcCommand vercommand = new OdbcCommand(str, con))
            {
                vercommand.Parameters.AddWithValue("?productname", drpitemslist.SelectedItem.Value);
                using(OdbcDataReader reader = vercommand.ExecuteReader())
                {
                    if(reader.Read())
                    {
                        name = reader["name"].ToString();
                    }

                    if(name == "")
                    {
                        updatestocktextbox();

                    }
                    else
                    {
                        checkstock();

                    }

                }

            }
        }

    }
    protected void Unnamed_Click(object sender, EventArgs e)
    {

    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        populategridview();
        addsalestotemparytable();
    }
    protected void GridProduct_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int index = Convert.ToInt32(e.RowIndex);
        DataTable dt = ViewState["dt"] as DataTable;
        dt.Rows[index].Delete();
        ViewState["dt"] = dt;

        for(int i = 0; i < GridProduct.Rows.Count; i++)
        {
            dedview = Int64.Parse(GridProduct.Rows[i].Cells[4].Text) * Int64.Parse(GridProduct.Rows[i].Cells[6].Text);
            balnewd += dedview;
            txtfinalbill.Text = balnewd.ToString();

            //deleting the same row from the temporary sales table in the database
            try
            {

                int rownumber = 0;
                string deletequery = "DELETE FROM temsalesitems  WHERE productname=? AND code=? AND rownumber=?";
                using(OdbcConnection con = DBConnection.getconnectionString(lblerror.Text))
                {
                    rownumber = index + 1;
                    con.Open();
                    using(OdbcCommand comm = new OdbcCommand(deletequery, con))
                    {
                        comm.Parameters.AddWithValue("?rownumber", rownumber);
                        comm.Parameters.AddWithValue("?stock", GridProduct.Rows[i].Cells[5].Text);
                        comm.Parameters.AddWithValue("?productname", GridProduct.Rows[i].Cells[2].Text);

                        comm.ExecuteNonQuery();
                    }
                }
            }
            catch(Exception)
            {

                throw;
            }
            BindGrid();
            int sum = 0;
            int labeltotal = 0;
            if(GridProduct.Rows.Count == 0)
            {

                labeltotal = sum;
                GridProduct.FooterRow.Cells[6].Text = "Total";
                GridProduct.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                GridProduct.FooterRow.Cells[7].Text = labeltotal.ToString("N2");
                txtfinalbill.Text = labeltotal.ToString("D");
            }
            else
            {

                sum = Convert.ToInt32(balnewd.ToString());
                labeltotal = sum;
                GridProduct.FooterRow.Cells[6].Text = "Total";
                GridProduct.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                GridProduct.FooterRow.Cells[7].Text = labeltotal.ToString("N2");
                txtfinalbill.Text = labeltotal.ToString("D");
            }
        }


    }
    protected void GridProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnsavepurchase_Click1(object sender, EventArgs e)
    {

        savePurchases();
        deletetemporarysale();
        DataTable dt = new DataTable();
        dt.Columns.AddRange(new DataColumn[7] { new DataColumn("ProductName"), new DataColumn("Category"), new DataColumn("Code"), new DataColumn("Stock"), new DataColumn("Quantity"), new DataColumn("Price"), new DataColumn("Total") });
        ViewState["dt"] = dt;
        this.BindGrid();
        ModalPopupExtender1.Show();
        //ShowMessage("Sales was saves successfully", EnumMessages.MessageType.Success);

    }
    protected void btncancelpurchase_Click1(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();

        dt.Columns.AddRange(new DataColumn[7] { new DataColumn("ProductName"), new DataColumn("Category"), new DataColumn("Code"), new DataColumn("Stock"), new DataColumn("Quantity"), new DataColumn("Price"), new DataColumn("Total") });

        ViewState["dt"] = dt;
        this.BindGrid();
        ShowMessage("Sales has been Cancelled", EnumMessages.MessageType.Error);
    }
    protected void GridProduct_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridProduct.EditIndex = e.NewEditIndex;
        this.BindGrid();
    }
    protected void GridProduct_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int total = 0;

        GridViewRow row = GridProduct.Rows[e.RowIndex];
        string quantity = (row.Cells[6].Controls[0] as TextBox).Text;
        total = Convert.ToInt32(quantity) * Convert.ToInt32((GridProduct.Rows[e.RowIndex].Cells[4]).Text);
        GridProduct.Rows[e.RowIndex].Cells[7].Text = total.ToString();
        GridProduct.Rows[e.RowIndex].Cells[6].Text = quantity.ToString();
        GridProduct.EditIndex = -1;
        calculateSum();
    }
    protected void GridProduct_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridProduct.EditIndex = -1;
        calculateSum();
        this.BindGrid();
    }
    protected void btngeneratereceipt_Click(object sender, EventArgs e)
    {

    }
}