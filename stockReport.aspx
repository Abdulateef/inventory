﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="stockReport.aspx.cs" Inherits="stockReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Stock Report</title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="bootstrap/fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="Styles/styles.css" rel="stylesheet" />
    <link href="Styles/LandingPage.css" rel="stylesheet" />
</head>

<body>
    <form id="form1" runat="server">
    <div class="row">
        <div class="container">
            <div class=" col-sx-6 col-md-10 col-md-offset-1" style="margin-top: 1%;">
                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" ShowPrintButton="true" ShowZoomControl="true" Height="900px" Width="900px">
                    <LocalReport ReportPath="stockPreview.rdlc">
                        <DataSources>
                            <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                        </DataSources>
                    </LocalReport>
                </rsweb:ReportViewer>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="ReportsTableAdapters.DataTable1TableAdapter"></asp:ObjectDataSource>
            </div>

        </div>
    </div>
        
    </form>
    <script src="Scripts/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.dropdown-submenu a.test').on("click", function (e) {
                $(this).next('ul').toggle();
                e.stopPropagation();
                e.preventDefault();
            });
        });
    </script>
</body>
</html>
