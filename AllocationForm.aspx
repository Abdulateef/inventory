﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AllocationForm.aspx.cs" Inherits="AllocationForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="Styles/styles.css" rel="stylesheet" />
    <link href="Styles/Pretty-Registration-Form.css" rel="stylesheet" />
    <link href="Content/bootstrap-theme.min.css" rel="stylesheet" />
    <script src="Scripts/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/alertmessages.js"></script>
    <link href="Styles/LandingPage.css" rel="stylesheet" />

    <script type="text/javascript">
        function MouseEvents(objRef, evt) {
            var checkbox = objRef.getElementsByTagName("input")[0];
            if (evt.type == "mouseover") {
                objRef.style.backgroundColor = "orange";
            }
            else {
                if (checkbox.checked) {
                    objRef.style.backgroundColor = "aqua";
                }
                else if (evt.type == "mouseout") {
                    if (objRef.rowIndex % 2 == 0) {
                        //Alternating Row Color
                        objRef.style.backgroundColor = "#C2D69B";
                    }
                    else {
                        objRef.style.backgroundColor = "white";
                    }
                }
            }
        }

        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                //Get the Cell To find out ColumnIndex
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        //If the header checkbox is checked
                        //check all checkboxes
                        //and highlight all rows
                        row.style.backgroundColor = "aqua";
                        inputList[i].checked = true;
                    }
                    else {
                        //If the header checkbox is checked
                        //uncheck all checkboxes
                        //and change rowcolor back to original
                        if (row.rowIndex % 2 == 0) {
                            //Alternating Row Color
                            row.style.backgroundColor = "#C2D69B";
                        }
                        else {
                            row.style.backgroundColor = "white";
                        }
                        inputList[i].checked = false;
                    }
                }
            }
        }
    </script>
    <script type="text/javascript">
        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            return false;
        }
    </script>
    <style>
        body {
            zoom: 110%;
        }

        hr.style5 {
            background-color: #fff;
            border-top: 2px dashed #8c8b8b;
        }

        .Background {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .Popup {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 400px;
            height: 350px;
        }

        .lbl {
            font-size: 16px;
            font-style: italic;
            font-weight: bold;
        }

        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
        }

        .modalPopup {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #0DA9D0;
            padding: 0;
        }

            .modalPopup .header {
                background-color: #2FBDF1;
                height: 30px;
                color: White;
                line-height: 30px;
                text-align: center;
                font-weight: bold;
            }

            .modalPopup .body {
                min-height: 50px;
                line-height: 30px;
                text-align: center;
                font-weight: bold;
                margin-bottom: 5px;
            }

        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .modalPopup {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 140px;
        }

        .messagealert {
            width: 30%;
            position: fixed;
            top: 0px;
            z-index: 100000;
            padding: 0;
            font-size: 15px;
            margin-right: 30%;
        }

        .dropdown-submenu {
            position: relative;
        }

            .dropdown-submenu .dropdown-menu {
                top: 0;
                left: 100%;
                margin-top: -1px;
            }
    </style>
</head>
<body>
    <div class="container">
        <form id="form1" runat="server">
            <div class="messagealert" id="alert_container">
            </div>
            <div id="modalPopUp" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">
                                <span id="spnTitle"></span>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                <span id="spnMsg"></span>.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="btnConfirm" class="btn btn-danger">
                                Yes, please</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 col-xs-12">
                    <div class="panel panel-primary ">
                        <div class="panel-heading">Allocated Items</div>
                        <div class="panel-body">
                            <div class="form-group col-md-offset-2">
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtdate">Date</label>
                                </div>
                                <div class="col-md-4 input-column">
                                    <asp:TextBox runat="server" ID="txtdate" CssClass="form-control" required="true" TextMode="Date"></asp:TextBox>
                                </div>
                                <div class="col-xs-4 col-md-4">
                                    <asp:Button runat="server" ID="btnloaddata" CssClass="btn btn-sm btn-primary" Text="Load Items" OnClick="btnloaddata_Click" />
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="table-responsive" style="height: 300px; width: 800px; overflow: auto; margin-left: 150px; text-align: center;">
                                <asp:GridView ID="Gridallocation" runat="server" BackColor="White" ShowHeaderWhenEmpty="True" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CssClass="table table-striped table-hover" CellSpacing="1" AlternatingRowStyle-BackColor="#C2D69B" Width="722px" AutoGenerateColumns="False" ViewStateMode="Enabled" OnRowDataBound="RowDataBound" OnSelectedIndexChanged="Gridallocation_SelectedIndexChanged" ShowFooter="false">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkCtrl" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="name" HeaderText="Name" SortExpression="name" ReadOnly="true" />
                                        <asp:BoundField DataField="code" HeaderText="Code" SortExpression="code" ReadOnly="true" />
                                        <asp:BoundField DataField="category" HeaderText="Category" SortExpression="category" ReadOnly="true" />
                                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                                        <asp:BoundField DataField="costprice" HeaderText="Price" SortExpression="costprice" ReadOnly="true" />
                                        <asp:BoundField DataField="total" HeaderText="Total" SortExpression="total" DataFormatString="{0:N2}" ReadOnly="true" />
                                        <asp:BoundField DataField="user" HeaderText="User" SortExpression="user" ReadOnly="true" />
                                        <asp:BoundField DataField="date" HeaderText="Date" SortExpression="date" ReadOnly="true" />

                                    </Columns>
                                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                                    <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#594B9C" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#33276A" />
                                </asp:GridView>
                            </div>
                            <hr />
                            <div class="form-group">
                                <div class="row col-md-8 col-md-offset-5">

                                    <div class="col-md-3 col-xs-3 col-md-pull-1">
                                        <asp:LinkButton ID="btnaccept" runat="server" CssClass="btn btn-primary" OnClientClick="return getConfirmation(this, 'Please confirm','Are you sure you want to accept the allocation?');" OnClick="btnaccept_Click"><i class="glyphicon glyphicon-download"></i>&nbsp;Accept</asp:LinkButton>
                                    </div>

                                    <div class=" col-md-3 col-xs-3 col-md-pull-2">
                                        <asp:LinkButton ID="btnrejectallocation" runat="server" CssClass="btn btn-danger" OnClientClick="return getConfirmation(this, 'Please confirm','Are you sure you want to reject the allocated items?');" OnClick="btnrejectallocation_Click"><i class="glyphicon glyphicon-trash"></i>&nbsp;Reject</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="style5" />
            <div class="row ">
                <div class="col-md-12 col-xs-12">
                    <div class="panel panel-primary ">
                        <div class="panel-heading">View Accepted Items</div>
                        <div class="panel-body">
                            <div class="table-responsive" style="height: 200px; width: 800px; overflow: auto; margin-left: 150px; text-align: center;">
                                <asp:GridView ID="gridaccepteditems" runat="server" BackColor="White" ShowHeaderWhenEmpty="True" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CssClass="table table-striped table-hover" CellSpacing="1" AlternatingRowStyle-BackColor="#C2D69B" Width="722px" AutoGenerateColumns="False" ViewStateMode="Enabled" OnRowDeleting="gridaccepteditems_RowDeleting" OnRowDataBound="gridaccepteditems_RowDataBound" OnSelectedIndexChanged="gridaccepteditems_SelectedIndexChanged" ShowFooter="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="name" HeaderText="Name" SortExpression="name" ReadOnly="true" />
                                        <asp:BoundField DataField="code" HeaderText="Code" SortExpression="code" ReadOnly="true" />
                                        <asp:BoundField DataField="category" HeaderText="Category" SortExpression="category" ReadOnly="true" />
                                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                                        <asp:BoundField DataField="costprice" HeaderText="Price" SortExpression="costprice" ReadOnly="true" />
                                        <asp:BoundField DataField="total" HeaderText="Total" SortExpression="total" DataFormatString="{0:N2}" ReadOnly="true" />
                                        <asp:BoundField DataField="user" HeaderText="User" SortExpression="user" ReadOnly="true" />
                                         <asp:BoundField DataField="date" HeaderText="Date" SortExpression="date" ReadOnly="true" />
                                        <%-- <asp:CommandField ShowDeleteButton="True" ControlStyle-CssClass="btn btn-sm btn-danger"/>--%>
                                    </Columns>
                                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                                    <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#594B9C" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#33276A" />
                                </asp:GridView>
                            </div>
                            <hr />
                            <div class="form-group">
                                <div class="row col-md-8 col-md-offset-5">

                                    <div class="col-md-3 col-xs-3 col-md-pull-1">
                                        <asp:LinkButton ID="btnsaveallocation" runat="server" CssClass="btn btn-primary" OnClientClick="return getConfirmation(this, 'Please confirm','Are you sure you want to save allocated items?');" OnClick="btnsaveallocation_Click"><i class="glyphicon glyphicon-upload"></i>&nbsp;Save</asp:LinkButton>
                                    </div>

                                    <div class=" col-md-3 col-xs-3 col-md-pull-2">
                                        <asp:LinkButton ID="btncancelallocation" runat="server" CssClass="btn btn-danger" OnClientClick="return getConfirmation(this, 'Please confirm','Are you sure you want to cancel  allocation?');" OnClick="btncancelallocation_Click"><i class="glyphicon glyphicon-erase"></i>&nbsp;Cancel</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script src="Scripts/jquery-1.9.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
</body>
</html>
