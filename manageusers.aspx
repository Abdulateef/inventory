﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="manageusers.aspx.cs" Inherits="manageusers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Added Items</title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="Styles/styles.css" rel="stylesheet" />
    <link href="Styles/LandingPage.css" rel="stylesheet" />
    <link href="Content/bootstrap-theme.min.css" rel="stylesheet" />
    <script src="Scripts/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/alertmessages.js"></script>


    <script type="text/javascript">
        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            return false;
        }
    </script>
    <style>
        .GridPager a,
        .GridPager span {
            display: inline-block;
            padding: 0px 9px;
            margin-right: 4px;
            border-radius: 3px;
            border: solid 1px #c0c0c0;
            background: #e9e9e9;
            box-shadow: inset 0px 1px 0px rgba(255,255,255, .8), 0px 1px 3px rgba(0,0,0, .1);
            font-size: .875em;
            font-weight: bold;
            text-decoration: none;
            color: #717171;
            text-shadow: 0px 1px 0px rgba(255,255,255, 1);
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background: #616161;
            box-shadow: inset 0px 0px 8px rgba(0,0,0, .5), 0px 1px 0px rgba(255,255,255, .8);
            color: #f0f0f0;
            text-shadow: 0px 0px 3px rgba(0,0,0, .5);
            border: 1px solid #3AC0F2;
        }

        .messagealert {
            width: 30%;
            position: fixed;
            top: 0px;
            z-index: 100000;
            padding: 0;
            font-size: 15px;
            margin-right: 30%;
        }

        .dropdown-submenu {
            position: relative;
        }

            .dropdown-submenu .dropdown-menu {
                top: 0;
                left: 100%;
                margin-top: -1px;
            }
    </style>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span class=" fa fa-briefcase"></span>Inventory Application</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Manual</a></li>


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="itemsalesReportviewer.aspx"><span class="fa fa-file"></span>|Sales and Receipt</a></li>
                        <li><a href="stockReport.aspx"><span class="fa fa-file"></span>|Item Reports</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><span class="fa fa-file"></span>|Other Reports</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tools<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="mastermenu.aspx"><span class="glyphicon glyphicon-cog"></span>|Setting</a></li>
                        <li class="dropdown-submenu">
                            <a class="test" href="#"><span class="glyphicon glyphicon-refresh"></span>|Synchronization <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#"><span class="glyphicon glyphicon-upload"></span>|Upload</a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-download"></span>|Download</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span class="glyphicon glyphicon-th-list"></span>|ROL</a></li>
                        <li><a href="QuikItemView.aspx"><span class="glyphicon glyphicon-eye-open"></span>|Stock Preview</a></li>
                        <%--  <li><a href="#"><span class="glyphicon glyphicon-eye-open"></span>|Prouct Preview</a></li>--%>
                        <li class="divider"></li>
                        <li class="dropdown-submenu">
                            <a class="test" href="#"><span class="glyphicon glyphicon-tasks"></span>|Item Management<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="AddnewItem.aspx"><span class="glyphicon glyphicon-plus"></span>|Add Item</a></li>
                                <li><a href="QuikItemView.aspx"><span class="glyphicon glyphicon-plus"></span>|View Added Items</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-right">
                <div class="form-group">
                    <a href="login.aspx" class="btn btn-primary">Log Out</a>
                    <%--<input type="datetime" class="form-control" placeholder="Search"/>--%>
                </div>

            </form>
        </div>
    </nav>
    <div class="container">
        <div class="messagealert" id="alert_container">
        </div>
        <form id="form1" runat="server" role="form">
            <div id="modalPopUp" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">
                                <span id="spnTitle"></span>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                <span id="spnMsg"></span>.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="btnConfirm" class="btn btn-danger">
                                Yes, please</button>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Label runat="server"></asp:Label>
            <div class="row">
                <div class="panel panel-primary" style="margin-top: 5%">
                    <div class="panel panel-heading">
                        <h1 class="panel-title center-block">Manage Users</h1>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive" style="height: 500px; width: 900px; overflow: auto; margin: 0 auto; text-align: center;">
                            <asp:GridView ID="GridProduct" runat="server" BackColor="White" ShowHeaderWhenEmpty="True" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CssClass="table table-striped table-hover" CellPadding="3" CellSpacing="1" AllowPaging="true" AllowSorting="true" PageSize="10" OnRowDeleting="GridProduct_RowDeleting" AlternatingRowStyle-BackColor="#C2D69B" Width="722px" AutoGenerateColumns="False" AutoGenerateEditButton="true" ViewStateMode="Enabled" DataKeyNames="id" OnPageIndexChanging="GridProduct_PageIndexChanging" AutoGenerateDeleteButton="true" OnRowDataBound="GridProduct_RowDataBound" OnRowEditing="GridProduct_RowEditing" OnRowCancelingEdit="GridProduct_RowCancelingEdit" OnRowUpdating="GridProduct_RowUpdating" EmptyDataText="No records has been added." OnSelectedIndexChanged="GridProduct_SelectedIndexChanged">
                                <Columns>
                                    <asp:TemplateField HeaderText="User Name" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("username") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Eval("username") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PassWord" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPassword" runat="server" Text='<%# Eval("password") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtPassword" runat="server" Text='<%# Eval("password") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Full Name" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFullname" runat="server" Text='<%# Eval("fullname") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtFullname" runat="server" Text='<%# Eval("fullname") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lblemail" runat="server" Text='<%# Eval("email") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtemail" runat="server" Text='<%# Eval("email") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Master User" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lblmasteruser" runat="server" Text='<%# Eval("masteruser") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtmasteruser" runat="server" Text='<%# Eval("masteruser") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <%-- <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true" ItemStyle-Width="150" />--%>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" CssClass="GridPager" />
                                <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                                <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                                <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                                <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#594B9C" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#33276A" />
                            </asp:GridView>
                            <table border="1" style="border-collapse: collapse; width: 40%" class="table table-responsive">
                                <tr>
                                    <td style="width: 150px">User Name:<br />
                                        <asp:TextBox ID="txtusername" runat="server" Width="135" CssClass="form-control" />
                                    </td>
                                    <td style="width: 150px">PassWord:<br />
                                        <asp:TextBox ID="txtpassword" runat="server" Width="135" CssClass="form-control" />
                                    </td>
                                    <td style="width: 150px">Full Name:<br />
                                        <asp:TextBox ID="txtfullname" runat="server" Width="135" CssClass="form-control" />
                                    </td>
                                    <td style="width: 150px">Email:<br />
                                        <asp:TextBox ID="txtemail" CssClass="form-control" runat="server" Width="135" />
                                    </td>
                                    <td style="width: 150px">Master User:<br />
                                        <asp:TextBox ID="txtmasteruser" runat="server" Width="135" CssClass="form-control" />
                                    </td>
                                    <td style="width: 100px">
                                        <%--<asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="Insert" />--%>
                                        <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" CssClass="btn btn-primary" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="site-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Powered By Sekat Technologies © 2017</h5>
                        </div>
                        <div class="col-sm-6 social-icons"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter-square"></i></a><a href="#"><i class="fa fa-instagram"></i></a></div>
                    </div>
                </div>
            </footer>
        </form>
    </div>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/alertmessages.js"></script>

</body>
</html>
