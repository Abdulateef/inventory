﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class QuikItemView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!this.IsPostBack)
        {
            ViewState["Filter"] = "ALL";
            populateGridview();
            drdownlistsearchby.Items.Insert(0, "---Search By---");
        }
    }

    public void populateGridview()
    {
        string query = "";
        if( ViewState["Filter"].ToString() == "ALL")
        {
            query = DBqueries.updateaddeditemqueryall;
        }
        else
        {
            query = DBqueries.updateaddeditemquerysort;
        }
        string errormessage = "";
        try
        { 
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcCommand comm = new OdbcCommand(query, con))
                {
                    comm.Parameters.AddWithValue("name", ViewState["Filter"].ToString());
                    using(OdbcDataAdapter adapter = new OdbcDataAdapter())
                    {
                        adapter.SelectCommand = comm;
                        using(DataSet dt = new DataSet())
                        {
                            adapter.Fill(dt);
                            gidviewitem.DataSource = dt.Tables[0];
                            gidviewitem.DataBind();
                            DropDownList ddlName = (DropDownList)gidviewitem.HeaderRow.FindControl("ddlName");
                            this.BindCountryList(ddlName);
                        }
                    }
                }
            }
        }
        catch(Exception)
        {

            throw;
        }
    }


    private void BindCountryList(DropDownList ddlName)
    {
        String query = "SELECT DISTINCT name FROM items";
        string message = "";
       
        try
        {
            using(OdbcConnection con = DBConnection.getconnectionString(message))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(query, con))
                {
                    ddlName.DataSource = com.ExecuteReader();
                    ddlName.DataTextField = "name";
                    ddlName.DataTextField = "name";
                    ddlName.DataBind();
                    con.Close();
                    ddlName.Items.Insert(0, new ListItem("--Select Product--", "0"));
                   // ddlName.Items.FindByValue(ViewState["Filter"].ToString()).Selected = true;
                }
            }
            
            
        }
        catch(Exception ex)
        {

            throw;
        }
        
    }
 
    protected void GridProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void drpitemslist_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void drdownlistsearchby_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void txtsearch_TextChanged(object sender, EventArgs e)
    {
        if(drdownlistsearchby.SelectedItem.Text == "Name")
        {
            (gidviewitem.DataSource as DataTable).DefaultView.RowFilter = string.Format("Name LIKE '{0}%'", txtsearch.Text);
        }
        else if(drdownlistsearchby.SelectedItem.Value == "Code")
        {
            (gidviewitem.DataSource as DataTable).DefaultView.RowFilter = string.Format("Code LIKE '{0}%'", txtsearch.Text);
        }
    }
    protected void ddlName_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;
        ViewState["Filter"] = ddlName.SelectedValue.ToString();
        this.populateGridview();
    }
    protected void gidviewitem_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gidviewitem_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gidviewitem.PageIndex = e.NewPageIndex;
        this.populateGridview();
    }
}