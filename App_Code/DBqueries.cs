﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Author: Abdulateef
/// This layer contaions all neccessary queies for the application
/// </summary>
public class DBqueries
{
	public  DBqueries()
	{
		
	}
    public static string checkuserquery = "SELECT * FROM users WHERE username =? OR email=?";
    public static string registeruserquery = "INSERT INTO users (username, fullname, email, password, masteruser) VALUES (?,?,?,?,?)";
    public static string loginquery = "SELECT masteruser, username, email FROM users WHERE username=? AND password =?";
    public static string recoverpasswordquery = "SELECT Username, Password FROM users WHERE email =?";
    public static string updatedropdwonlistquery = "SELECT id,  name, code  FROM items";
    public static string updatedropdwonlistquery2 = "SELECT id,  name, code  FROM purchase";
    public static string savepurchasequries = "INSERT INTO sales(type,name,contact,total, final) VALUES(?,?,?,?,?)";
    public static string insertrptitemsoldofflinequery =  "INSERT INTO rptitemsoldoffline(name,type,contact, code, product, quantity,price, total, final) VALUES(?,?,?,?,?,?,?,?,?)";
    public static string updateitem = "UPDATE items SET name=?,category=?,s_price=?,c_price=? WHERE id=? AND code=?;";
    public static string insertintopurchase = "INSERT INTO purchase (name, code, category, quantity,costprce, user) VALUES (?,?,?,?,?,?)";
    public static string insertintoreturngoods = "INSERT INTO returnitem (name, category, narration, total,quantity, date,code, sellinprice, user) VALUES (?,?,?,?,?,?,?,?,?)";
    public static string deleteFromPurchase = "DELETE FROM purchase WHERE name=?, code=?, category=?, quantity=?,costprce=? AND invoicenumber=?";
    public static string insertIntoItems = "INSERT INTO items (name, code, category,s_price, c_price,status, ROL) VALUES (?,?,?,?,?,?,?)";
    public static string deleteFromItems = "DELETE FROM  items WHERE name=?, code=?, category=?,s_price=?, c_price=?,status=?, ROL=?)";

    public static string temsalesitemsquery = "INSERT INTO `temsalesitems`( `rownumber`, `code`, `productname`, `category`, `price`, `stock`, `quantty`, `total`) VALUES (?,?,?,?,?,?,?,?)";

    public static string updateaddeditemquery = "Select id, code,  name, category, s_price, stock, ROL from items";

    public static string updateaddeditemqueryall = "Select id, code,  name, category, s_price, stock, ROL from items";
    public static string updateaddeditemquerysort = "Select id, code,  name, category, s_price, stock, ROL from items WHERE name=?";
    public static string selectusersquery = "SELECT id, username, password, fullname, email, masteruser FROM users";
    public static string insertnewuser = "INSERT INTO users (rownumber, username, password, fullname,email, masteruser) VALUES (?,?,?,?,?,?)";
    public static string updateuser = "UPDATE users SET username=?, password=?,fullname=?,email=?,masteruser=? WHERE id=?";
    public static string deleteuser = "DELETE FROM users WHERE rownumber=?";
    public static string deletesalesreport = "DELETE FROM salesreport";
    public static string deletereceipt = "DELETE FROM receipt";
    public static string deletetemsalesitems = "DELETE FROM  temsalesitems";
    public static string updatepurchasedropdwonlistquery = "SELECT id,  name FROM purchase";
    public static string insertintoreceipt = "INSERT INTO receipt  (Name, Price, Quantity, Total) VALUES (?,?,?,?)";
    public static string getallocateditem = "SELECT `name`,`code`,`category`,`quantity`,`costprice`,`total`,`user`, `date` FROM `allocation`";
    public static string getallocateditemByDate = "SELECT `name`,`code`,`category`,`quantity`,`costprice`,`total`,`user` FROM `allocation` WHERE date=?";
    public static string savesales = "INSERT INTO salesreport(name,code, quantity,total) VALUES(?,?,?,?)";
}