﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Odbc;
using System.Data;
using System.Web;

/// <summary>
/// this class is to connect the application to the data layer in mysql
/// </summary>
public  class DBConnection
{
	public DBConnection()
	{
        
		//		// TODO: Add constructor logic here
		//
	}

    public static OdbcConnection getconnectionString(string errormessge)
    {
        try
        {

            string connectionstring = "DRIVER={MySQL ODBC 3.51 Driver};Server=localhost;Database=micmacpo_inventory;User Id=root;Password=";
            OdbcConnection con = new OdbcConnection(connectionstring);
            return con;
            
        }
        catch(Exception ex)
        {
            errormessge = "Oops an error occured while performing your action, Please try again";
            return null;
            
        }

    }
}