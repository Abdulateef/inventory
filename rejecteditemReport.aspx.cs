﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data.Odbc;

public partial class rejecteditemReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string dtfrom;
        string dtto;
        //string datee;

        dtfrom = Convert.ToDateTime(txtfrom.Text).ToString("yyyy-MM-dd");
        dtto = Convert.ToDateTime(txttto.Text).ToString("yyyy-MM-dd");

        string date = "%" + dtto + "%";
        ReportViewer1.ProcessingMode = ProcessingMode.Local;
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/rejecteditem.rdlc");
        rejectectitem rejectectitem = GetData("select * from  returnitem WHERE date between'" + dtfrom + "' and '" + dtto + "' OR `date` LIKE '" + date + "'");
        ReportDataSource datasource = new ReportDataSource("DataSet1", rejectectitem.Tables[0]);
        ReportViewer1.LocalReport.DataSources.Clear();
        ReportViewer1.LocalReport.DataSources.Add(datasource);
    }

    private rejectectitem GetData(string query)
    {
        try
        {
            string errormessage = "";
            OdbcCommand com = new OdbcCommand(query);
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcDataAdapter sda = new OdbcDataAdapter())
                {
                    com.Connection = con;
                    sda.SelectCommand = com;
                    using(rejectectitem rejectectitem = new rejectectitem())
                    {
                        sda.Fill(rejectectitem, "DataTable1");
                        return rejectectitem;
                    }
                }

            }
        }
        catch(Exception)
        {

            throw;
        }
    }
}