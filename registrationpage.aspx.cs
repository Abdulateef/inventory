﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Odbc;
using System.Data;

public partial class registrationpage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        lblerror.Visible = false;
        
    }
    protected void ShowMessage(string Message, EnumMessages.MessageType type)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
    }

    protected void btnregister_Click(object sender, EventArgs e)
    {
        checkUserAndEmail();
        registerUser();
    }

    public void checkUserAndEmail()
    {
        int rowcount = 0;
       string checkuserquery =  DBqueries.checkuserquery;
        //string errormessage = "";
        try
        {
            using( OdbcConnection con = DBConnection.getconnectionString(lblerror.Text))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(checkuserquery, con))
                {
                    com.Parameters.Add("@username", OdbcType.VarChar, 250).Value = txtname.Text.Trim();
                    com.Parameters.Add("@email", OdbcType.VarChar, 250).Value = txtemail.Text.Trim();
                    OdbcDataAdapter adt = new OdbcDataAdapter(com);
                    DataTable dt = new DataTable();
                    adt.Fill(dt);
                    com.ExecuteNonQuery();
                    string message = string.Empty;
                    rowcount = dt.Rows.Count;
                    if(rowcount > 0)
                    {
                        //message = "Username Or Email already exists.\\nPlease choose a different username or email.";
                        //lblerror.ForeColor = System.Drawing.Color.Red;
                        //lblerror.Text = "Username Or Email already exists";
                        //ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                        ShowMessage("Username Or Email already exists.\\nPlease choose a different username or email.", EnumMessages.MessageType.Info);
                    }
                   
                }
                
            }
        }
        catch(Exception ex)
        {
            //lblerror.ForeColor = System.Drawing.Color.Red;
            //lblerror.Text = "Oops An error occured"; 
            ShowMessage("Oops An error occured", EnumMessages.MessageType.Info);
        }
    }

    public void registerUser()
    {
        string registeruserquery = DBqueries.registeruserquery;
        try
        {
            int rowcount = 0;
            using(OdbcConnection con = DBConnection.getconnectionString(lblerror.Text))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(registeruserquery,con))
                {
                    com.Parameters.AddWithValue("?username", txtname.Text.ToString());
                    com.Parameters.AddWithValue("?fullname",txtfullname.Text.ToString());
                    com.Parameters.AddWithValue("?email", txtemail.Text.ToString());
                    com.Parameters.AddWithValue("?password", txtpassword.Text.ToString());
                    com.Parameters.AddWithValue("?masteruser", drpduserrow.Text.ToString());
                    rowcount = com.ExecuteNonQuery();

                    if(rowcount > 0 )
                    {
                        //lblerror.Text = "User has been Created";
                        //lblerror.ForeColor = System.Drawing.Color.Blue;
                        ShowMessage("User has been Created", EnumMessages.MessageType.Success);
                        
                    }
                }

                
            }
            cleartxt();
        }
        catch(Exception ex)
        {

            //lblerror.Text = "Oops an error occured";
            ShowMessage("Oops an error occured", EnumMessages.MessageType.Error);
        }
    }

    public void cleartxt()
    {
        txtcfmpassword.Text = "";
        txtemail.Text = "";
        txtname.Text = "";
        txtfullname.Text = "";
        txtpassword.Text = "";
        txtpassword.Text = "";
    }
    protected void btntest_Click(object sender, EventArgs e)
    {
        checkUserAndEmail();
        registerUser();
    }
}