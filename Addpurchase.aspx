﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Addpurchase.aspx.cs" Inherits="Addpurchase" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manage Purchase</title>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="bootstrap/fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="Styles/styles.css" rel="stylesheet" />
    <link href="Styles/LandingPage.css" rel="stylesheet" />
    <script src="Scripts/alertmessages.js"></script>

    <script type="text/javascript">
        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            return false;
        }
    </script>
    <style>
        hr.style5 {
            background-color: #fff;
            border-top: 2px dashed #8c8b8b;
        }



        .messagealert {
            width: 30%;
            position: fixed;
            top: 0px;
            z-index: 100000;
            padding: 0;
            font-size: 15px;
            margin-right: 30%;
        }


        .dropdown-submenu {
            position: relative;
        }

            .dropdown-submenu .dropdown-menu {
                top: 0;
                left: 100%;
                margin-top: -1px;
            }
    </style>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span class=" fa fa-briefcase"></span>Inventory Application</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Manual</a></li>


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="itemsalesReportviewer.aspx"><span class="fa fa-file"></span>|Sales and Receipt</a></li>
                        <li><a href="stockReport.aspx"><span class="fa fa-file"></span>|Item Reports</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><span class="fa fa-file"></span>|Other Reports</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tools<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="mastermenu.aspx"><span class="glyphicon glyphicon-cog"></span>|Setting</a></li>
                        <li class="dropdown-submenu">
                            <a class="test" href="#"><span class="glyphicon glyphicon-refresh"></span>|Synchronization <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#"><span class="glyphicon glyphicon-upload"></span>|Upload</a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-download"></span>|Download</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span class="glyphicon glyphicon-th-list"></span>|ROL</a></li>
                        <li><a href="QuikItemView.aspx"><span class="glyphicon glyphicon-eye-open"></span>|Stock Preview</a></li>
                        <%--  <li><a href="#"><span class="glyphicon glyphicon-eye-open"></span>|Prouct Preview</a></li>--%>
                        <li class="divider"></li>
                        <li class="dropdown-submenu">
                            <a class="test" href="#"><span class="glyphicon glyphicon-tasks"></span>|Item Management<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="AddnewItem.aspx"><span class="glyphicon glyphicon-plus"></span>|Add Item</a></li>
                                <li><a href="QuikItemView.aspx"><span class="glyphicon glyphicon-plus"></span>|View Added Items</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-right">
                <div class="form-group">
                    <a href="login.aspx" class="btn btn-primary">Log Out</a>
                    <%--<input type="datetime" class="form-control" placeholder="Search"/>--%>
                </div>

            </form>
        </div>
    </nav>
    <br />
    <br />
    <br />
    <%-- Content --%>
    <div class="container">
        <form id="form1" runat="server" class="form-horizontal">
            <div class="messagealert" id="alert_container">
            </div>
            <div id="modalPopUp" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">
                                <span id="spnTitle"></span>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                <span id="spnMsg"></span>.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="btnConfirm" class="btn btn-danger">
                                Yes, please</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 col-xs-12">
                    <div class="panel panel-primary ">
                        <div class="panel-heading">Add Purchase</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtname">Name</label>
                                </div>
                                <div class="col-sm-3 input-column">
                                    <asp:DropDownList runat="server" ID="drpitemslist" CssClass="form-control" OnSelectedIndexChanged="drpitemslist_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtname">Suppier Name</label>
                                </div>
                                <div class="col-sm-3 input-column">
                                    <asp:TextBox runat="server" ID="txtsuppliername" CssClass="form-control" required="true" Placeholder="Enter Supplier Name"></asp:TextBox>
                                </div>
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtcode">Code</label>
                                </div>
                                <div class="col-sm-2 input-column">
                                    <asp:TextBox runat="server" ID="txtcode" CssClass="form-control" Enabled="false" required="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtQauntity">Qauntity</label>
                                </div>
                                <div class="col-sm-3 input-column">
                                    <asp:TextBox runat="server" ID="txtQauntity" CssClass="form-control" TextMode="Number" required="true" Placeholder="Enter Quantity"></asp:TextBox>
                                </div>

                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtinvoicenumber">Invoice Number</label>
                                </div>
                                <div class="col-sm-3 input-column">
                                    <asp:TextBox runat="server" ID="txtinvoicenumber" CssClass="form-control" required="true" Placeholder="Enter Invoice Number"></asp:TextBox>
                                </div>
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtdate">Date</label>
                                </div>
                                <div class="col-sm-3 input-column">
                                    <asp:TextBox runat="server" ID="txtdate" CssClass="form-control" required="true" Placeholder="Enter date" TextMode="Date"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtcostprice">Cost Price</label>
                                </div>
                                <div class="col-sm-3 input-column">
                                    <asp:TextBox runat="server" ID="txtcostprice" CssClass="form-control" required="true" Placeholder="Enter Cost Price"></asp:TextBox>
                                </div>

                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtsellingprice">Selling Price</label>
                                </div>
                                <div class="col-sm-3 input-column">
                                    <asp:TextBox runat="server" ID="txtsellingprice" CssClass="form-control" required="true" Placeholder="Enter Seliing Price"></asp:TextBox>
                                </div>
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtnarration">Nar</label>
                                </div>
                                <div class="col-sm-3 input-column">
                                    <asp:TextBox runat="server" ID="txtnarration" CssClass="form-control" required="true" Placeholder="Narration"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtcostprice">Pack/Unit</label>
                                </div>
                                <div class="col-sm-3 input-column">
                                    <asp:DropDownList runat="server" ID="drppackunit" CssClass="form-control" OnSelectedIndexChanged="drppackunit_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem>Unit</asp:ListItem>
                                        <asp:ListItem>Dozen</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtcategory">Category</label>
                                </div>
                                <div class="col-sm-3 input-column">
                                    <asp:TextBox runat="server" ID="txtcategory" CssClass="form-control" Enabled="false" required="true"></asp:TextBox>
                                </div>
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtamount">Total Amount</label>
                                </div>
                                <div class="col-sm-3 input-column">
                                    <asp:TextBox runat="server" ID="txtamount" CssClass="form-control" required="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row col-md-8 col-md-offset-5">

                                <div class="col-md-3 col-xs-3 col-md-pull-1">
                                    <asp:LinkButton ID="btnaddpurchase" runat="server" CssClass="btn btn-primary" OnClientClick="return getConfirmation(this, 'Please confirm','Are you sure you want to add Item?');" OnClick="btnaddpurchase_Click"><i class="glyphicon glyphicon-upload"></i>&nbsp;Add</asp:LinkButton>
                                </div>

                                <div class=" col-md-3 col-xs-3 col-md-pull-2">
                                    <asp:LinkButton ID="btncancelpurchase" runat="server" CssClass="btn btn-danger" OnClientClick="return getConfirmation(this, 'Please confirm','Are you sure you want to cancel  Purchase?');" OnClick="btncancelpurchase_Click"><i class="glyphicon glyphicon-erase"></i>&nbsp;Cancel</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="style5" />
            <div class="row ">
                <div class="col-md-6 col-xs-4 col-md-pull-0">
                    <div class="panel panel-primary ">
                        <div class="panel-heading">Remove Purhase</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-4 label-column">
                                    <label class="control-label" for="txtname">Name</label>
                                </div>
                                <div class="col-md-6 input-column">
                                    <asp:DropDownList runat="server" ID="ddlitemremove" CssClass="form-control" OnSelectedIndexChanged="ddlitemremove_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 label-column">
                                    <label class="control-label" for="txtcoderemove">Code</label>
                                </div>
                                <div class="col-md-6 input-column">
                                    <asp:TextBox runat="server" ID="txtcoderemove" CssClass="form-control" Enabled="false" required="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 label-column">
                                    <label class="control-label" for="txtinvoiceremove">Invoice No</label>
                                </div>
                                <div class="col-md-6 input-column">
                                    <asp:TextBox runat="server" ID="txtinvoiceremove" CssClass="form-control" required="true" Placeholder="Enter Invoice Number"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 label-column">
                                    <label class="control-label" for="txtquantityremove">Qauntity</label>
                                </div>
                                <div class="col-md-6 input-column">
                                    <asp:TextBox runat="server" ID="txtquantityremove" CssClass="form-control" required="true" Placeholder="Enter Quantity"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row col-md-5 col-md-offset-3">

                                <div class="col-md-3 col-xs-3 col-md-offset-2">
                                    <asp:LinkButton ID="btnremove" runat="server" CssClass="btn btn-primary" OnClientClick="return getConfirmation(this, 'Please confirm','Are you sure you want to remove item?');" OnClick="btnremove_Click"><i class="glyphicon glyphicon-trash"></i>&nbsp;Reomve</asp:LinkButton>
                                </div>

                                <div class=" col-md-3 col-xs-3 col-md-offset-4">
                                    <asp:LinkButton ID="btncancel" runat="server" CssClass="btn btn-danger" OnClick="btncancel_Click"><i class="glyphicon glyphicon-erase"></i>&nbsp;Cancel</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-4 ">
                    <div class="panel panel-primary ">
                        <div class="panel-heading">Manage Return Products and Gifts</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtname">Name</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:DropDownList runat="server" ID="drplistrgitems" CssClass="form-control" OnSelectedIndexChanged="drplistrgitems_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtcodergitem">Code</label>
                                </div>
                                <div class="col-sm-4 input-column">
                                    <asp:TextBox runat="server" ID="txtcodergitem" CssClass="form-control" Enabled="false" required="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtqauntityrgitem">Qty</label>
                                </div>
                                <div class="col-sm-3 input-column">
                                    <asp:TextBox runat="server" ID="txtqauntityrgitem" CssClass="form-control" placeholder="Quantity" TextMode="Number" required="true"></asp:TextBox>

                                </div>
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtrgcost">Cost</label>
                                </div>
                                <div class="col-sm-3 input-column">
                                    <asp:TextBox runat="server" ID="txtrgcost" CssClass="form-control" TextMode="Number" required="true"></asp:TextBox>

                                </div>
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtrgtotalamount">Toatal</label>
                                </div>
                                <div class="col-sm-3 input-column">
                                    <asp:TextBox runat="server" ID="txtrgtotalamount" CssClass="form-control" required="true"></asp:TextBox>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtrgnarration">Nar</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtrgnarration" CssClass="form-control" required="true" Placeholder="Enter narration"></asp:TextBox>
                                </div>
                                <div class="col-sm-1 label-column">
                                    <label class="control-label" for="txtrgdate">Date</label>
                                </div>
                                <div class="col-md-4 input-column">
                                    <asp:TextBox runat="server" ID="txtrgdate" CssClass="form-control" TextMode="Date" required="true" Placeholder="Enter Quantity"></asp:TextBox>
                                </div>
                            </div>
                              <div class="form-group">
                                <div class="col-sm-2 label-column">
                                    <label class="control-label" for="txtrgcategory">Category</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtrgcategory" CssClass="form-control" required="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row col-md-8 col-md-offset-3">

                                <div class="col-md-3 col-xs-3 col-md-offset-2">
                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-primary" OnClientClick="return getConfirmation(this, 'Please confirm','Are you sure you want to remove item?');" OnClick="btnremove_Click"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;Add</asp:LinkButton>
                                </div>

                                <div class=" col-md-3 col-xs-3 col-md-offset-1">
                                    <asp:LinkButton ID="btncancelreturnitem" runat="server" CssClass="btn btn-danger" OnClick="btncancelreturnitem_Click"><i class="glyphicon glyphicon-erase"></i>&nbsp;Cancel</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="site-footer" style="margin-top: 20%">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Powered By Sekat Technologies © 2017</h5>
                        </div>
                        <div class="col-sm-6 social-icons"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter-square"></i></a><a href="#"><i class="fa fa-instagram"></i></a></div>
                    </div>
                </div>
            </footer>
        </form>
    </div>
    <script src="Scripts/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.dropdown-submenu a.test').on("click", function (e) {
                $(this).next('ul').toggle();
                e.stopPropagation();
                e.preventDefault();
            });
        });
    </script>
</body>
</html>
