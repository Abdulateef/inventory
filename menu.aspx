﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="menu.aspx.cs" Inherits="menu" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Menu</title>

    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="Styles/styles.css" rel="stylesheet" />
    <link href="Styles/Pretty-Registration-Form.css" rel="stylesheet" />
    <link href="Content/bootstrap-theme.min.css" rel="stylesheet" />
    <script src="Scripts/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/alertmessages.js"></script>
    <link href="Styles/LandingPage.css" rel="stylesheet" />

    <script type="text/javascript">
        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            return false;
        }
    </script>
    <style>
        body {
            zoom: 110%;
        }

        .Background {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .Popup {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 400px;
            height: 350px;
        }

        .lbl {
            font-size: 16px;
            font-style: italic;
            font-weight: bold;
        }

        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
        }

        .modalPopup {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #0DA9D0;
            padding: 0;
        }

            .modalPopup .header {
                background-color: #2FBDF1;
                height: 30px;
                color: White;
                line-height: 30px;
                text-align: center;
                font-weight: bold;
            }

            .modalPopup .body {
                min-height: 50px;
                line-height: 30px;
                text-align: center;
                font-weight: bold;
                margin-bottom: 5px;
            }

        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .modalPopup {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 140px;
        }

        .messagealert {
            width: 30%;
            position: fixed;
            top: 0px;
            z-index: 100000;
            padding: 0;
            font-size: 15px;
            margin-right: 30%;
        }

        .dropdown-submenu {
            position: relative;
        }

            .dropdown-submenu .dropdown-menu {
                top: 0;
                left: 100%;
                margin-top: -1px;
            }
    </style>
</head>
<body>
    <%-- <form id="form1" runat="server">--%>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span class=" fa fa-briefcase"></span>Inventory Application</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Manual</a></li>


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="itemsalesReportviewer.aspx"><span class="fa fa-file"></span>|Sales and Receipt</a></li>
                        <li><a href="stockReport.aspx"><span class="fa fa-file"></span>|Item Reports</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><span class="fa fa-file"></span>|Other Reports</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tools<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="mastermenu.aspx"><span class="glyphicon glyphicon-cog"></span>|Setting</a></li>
                        <li class="dropdown-submenu">
                            <a class="test" href="#"><span class="glyphicon glyphicon-refresh"></span>|Synchronization <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#"><span class="glyphicon glyphicon-upload"></span>|Upload</a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-download"></span>|Download</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span class="glyphicon glyphicon-th-list"></span>|ROL</a></li>
                        <li><a href="QuikItemView.aspx"><span class="glyphicon glyphicon-eye-open"></span>|Stock Preview</a></li>
                        <%--  <li><a href="#"><span class="glyphicon glyphicon-eye-open"></span>|Prouct Preview</a></li>--%>
                        <li class="divider"></li>
                        <li class="dropdown-submenu">
                            <a class="test" href="#"><span class="glyphicon glyphicon-tasks"></span>|Item Management<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="AddnewItem.aspx"><span class="glyphicon glyphicon-plus"></span>|Add Item</a></li>
                                <li><a href="QuikItemView.aspx"><span class="glyphicon glyphicon-plus"></span>|View Added Items</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-right">
                <div class="form-group">
                    <a href="login.aspx" class="btn btn-primary">Log Out</a>
                    <%--<input type="datetime" class="form-control" placeholder="Search"/>--%>
                </div>

            </form>
        </div>
    </nav>
    <%-- </form>--%>
    <div class="container">
        <div class="messagealert" id="alert_container">
        </div>
        <form runat="server" id="form1" role="form">
            <div id="modalPopUp" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">
                                <span id="spnTitle"></span>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                <span id="spnMsg"></span>.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="btnConfirm" class="btn btn-danger">
                                Yes, please</button>
                        </div>
                    </div>
                </div>
            </div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <div>
                <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panl1" TargetControlID="btngeneratereceipt"
                    CancelControlID="Button2" BackgroundCssClass="Background">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="Panl1" runat="server" CssClass="Popup" align="center" Style="display: none">
                    <iframe style="width: 350px; height: 300px;" id="irm1" src="receipt.aspx" runat="server"></iframe>
                    <br />
                    <asp:Button ID="Button2" runat="server" Text="Close" />
                </asp:Panel>


            </div>
            <div>
                <asp:LinkButton ID="lnkDummy" runat="server"></asp:LinkButton>
                <cc1:ModalPopupExtender ID="ModalPopupExtender1" BehaviorID="mpe" runat="server"
                    PopupControlID="pnlPopup" TargetControlID="lnkDummy" BackgroundCssClass="modalBackground" CancelControlID="btngeneratereceipt">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                    <div class="header">Print Receipt</div>
                    <div class="body">
                        Do you want to want to generate Reciept?
                      
                     <br />
                        <asp:Button ID="btnHide" runat="server" Text="Cancel" CssClass="btn btn-sm btn-danger " />
                        <asp:Button ID="btngeneratereceipt" runat="server" Text="Yes" OnClick="btngeneratereceipt_Click" CssClass="btn btn-sm btn-primary" />
                    </div>
                </asp:Panel>
            </div>
            <%--   <div class="container">--%>
            <asp:Label runat="server" ID="lblerror"></asp:Label>
            <br />
            <br />
            <br />
            <div class="panel panel-primary">
                <div class="panel panel-heading">
                    <h1 class="panel-title">Item Sold</h1>
                </div>
                <div class="panel-body">
                    <div class="table-responsive" style="height: 200px; width: 900px; overflow: auto; margin-left: 80px; text-align: center;">
                        <asp:GridView ID="GridProduct" runat="server" BackColor="White" ShowHeaderWhenEmpty="True" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CssClass="table table-striped table-hover" CellSpacing="1" AlternatingRowStyle-BackColor="#C2D69B" Width="722px" OnRowDeleting="GridProduct_RowDeleting" OnRowEditing="GridProduct_RowEditing" OnRowUpdating="GridProduct_RowUpdating" OnRowCancelingEdit="GridProduct_RowCancelingEdit" AutoGenerateColumns="False" ViewStateMode="Enabled" OnSelectedIndexChanged="GridProduct_SelectedIndexChanged" ShowFooter="true">
                            <Columns>
                                <asp:TemplateField HeaderText="No.">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="code" HeaderText="Code" SortExpression="code" ReadOnly="true" />
                                <asp:BoundField DataField="ProductName" HeaderText="Product Name" SortExpression="ProductName" ReadOnly="true" />
                                <asp:BoundField DataField="category" HeaderText="Category" SortExpression="category" ReadOnly="true" />
                                <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" ReadOnly="true" />
                                <asp:BoundField DataField="Stock" HeaderText="Stock" SortExpression="Stock" ReadOnly="true" />
                                <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                                <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total" DataFormatString="{0:N2}" ReadOnly="true" />

                                <asp:CommandField ShowDeleteButton="True" ShowEditButton="true"/>
                                <%--<asp:CommandField  ItemStyle-Width="150" />--%>
                            </Columns>
                            <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                            <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                            <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                            <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#594B9C" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#33276A" />
                        </asp:GridView>
                    </div>
                    <div>
                        <asp:Label ID="lblresult" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h1 class="panel-title">Manual Add</h1>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                <label class="control-label" for="drpitems">Product</label>
                            </div>
                            <div class="col-sm-4 col-md-pull-1 input-column">
                                <asp:DropDownList runat="server" ID="drpitemslist" CssClass="form-control" OnSelectedIndexChanged="drpitemslist_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div class="col-sm-2 control-label">
                                <label class="control-label" for="txtcode">Code</label>
                            </div>
                            <div class="col-sm-3 col-md-pull-1 input-column">
                                <asp:TextBox runat="server" ID="txtcode" CssClass="form-control" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <div class="col-sm-3 control-label">
                                <label class="control-label" for="txtStock">Stock</label>
                            </div>
                            <div class="col-sm-3 col-md-pull-1 input-column">
                                <asp:TextBox runat="server" ID="txtStock" CssClass="form-control" Enabled="false"></asp:TextBox>
                            </div>
                            <div class="col-sm-2 control-label">
                                <label class="control-label" for="txtitemquantity">Quantity</label>
                            </div>
                            <div class="col-sm-3 col-md-pull-1 input-column ">
                                <asp:TextBox runat="server" ID="txtitemquantity" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
                            </div>
                            <div class="col-sm-3 input-column col-md-push-10">
                                <asp:Button runat="server" class="btn btn-primary" Text="Add" ID="btnAdd" OnClick="btnAdd_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h1 class="panel-title">Purchase Information</h1>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-1  control-label">
                                <label class="control-label" for="txtname">Name</label>
                            </div>
                            <div class="col-sm-3 input-column">
                                <asp:TextBox runat="server" ID="txtname" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-sm-1 control-label">
                                <label class="control-label" for="txtContact">Contact</label>
                            </div>
                            <div class="col-sm-3 input-column">
                                <asp:TextBox runat="server" ID="txtContact" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                            </div>
                            <br />
                            <div class="col-sm-1 label-column">
                                <label class="control-label" for="txtexchangerate">Exchange Rate:</label>
                            </div>
                            <div class="col-sm-3 input-column">
                                <asp:TextBox runat="server" ID="TextBox1" CssClass="form-control" placeholder="%"></asp:TextBox>
                            </div>
                            <%--<div class="col-sm-1 control-label col-md-pull-0">
                                <label style="font-weight: bold">%</label>
                            </div>--%>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <div class="col-sm-1 control-label">
                                <label class="control-label" for="txtitemquantity">Discount:</label>
                            </div>
                            <div class="col-sm-3 input-column">
                                <asp:TextBox runat="server" ID="TextBox2" CssClass="form-control" placeholder="%"></asp:TextBox>
                            </div>
                            <div class="col-sm-1 label-column">
                                <label class="control-label" for="txtfinalbill">Final Bill</label>
                            </div>
                            <div class="col-sm-3 input-column">
                                <asp:TextBox runat="server" ID="txtfinalbill" CssClass="form-control" placeholder="N"></asp:TextBox>
                            </div>
                            <div class="col-sm-1 label-column">
                                <label class="control-label" for="txtadvancement">Advanced Payment</label>
                            </div>
                            <div class="col-sm-3 input-column">
                                <asp:TextBox runat="server" ID="txtadvancement" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <div class="form-group">
                            <div class="col-sm-4 label-column center-block">
                            </div>
                            <div class="col-sm-6 input-column">
                                <label class="radio-inline">
                                    <asp:RadioButton runat="server" ID="rdbCash" Text="Cash" GroupName="transaction" />
                                </label>
                                <label class="radio-inline">
                                    <asp:RadioButton runat="server" ID="rdbCheck" Text="Check" GroupName="transaction" />
                                </label>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="panel panel-default">
                <%-- <div class="panel-heading">
                    <h1 class="panel-title">Saves</h1>
                </div>--%>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-4 label-column">
                        </div>
                        <div class="col-sm-2">
                            <asp:LinkButton ID="btnsavepurchase" runat="server" CssClass="btn btn-success" OnClientClick="return getConfirmation(this, 'Please confirm','Are you sure you want to save sales?');" OnClick="btnsavepurchase_Click1"><i class="glyphicon glyphicon-save"></i>&nbsp;Save Purchase</asp:LinkButton>
                        </div>

                        <div class="col-md-2 ">
                            <asp:LinkButton ID="btncancelpurchase" runat="server" CssClass="btn btn-danger" OnClientClick="return getConfirmation(this, 'Please confirm','Are you sure you want to Cancel sales?');" OnClick="btncancelpurchase_Click1"><i class="glyphicon glyphicon-trash"></i>&nbsp;Cancel Purchase</asp:LinkButton>
                        </div>

                    </div>
                </div>
            </div>
            <%-- </div>--%>
            <footer class="site-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Powered By Sekat Technologies © 2017</h5>
                        </div>
                        <div class="col-sm-6 social-icons"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter-square"></i></a><a href="#"><i class="fa fa-instagram"></i></a></div>
                    </div>
                </div>
            </footer>
        </form>
    </div>

    <script src="Scripts/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.dropdown-submenu a.test').on("click", function (e) {
                $(this).next('ul').toggle();
                e.stopPropagation();
                e.preventDefault();
            });
        });
    </script>
</body>
</html>
