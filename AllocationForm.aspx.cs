﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Odbc;

public partial class AllocationForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!this.IsPostBack)
        {
            BindData();
        }
    }

    // method to display error
    protected void ShowMessage(string Message, EnumMessages.MessageType type)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
    }

    public void GetSelectedRecords()
    {
        DataTable dt = new DataTable();
        dt.Columns.AddRange(new DataColumn[8] { new DataColumn("name"), new DataColumn("code"), new DataColumn("category"), new DataColumn("Quantity"), new DataColumn("costprice"), new DataColumn("total"), new DataColumn("user"), new DataColumn("date") });
        foreach(GridViewRow row in Gridallocation.Rows)
        {
            if(row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkCtrl = (row.Cells[0].FindControl("chkCtrl") as CheckBox);
                if(chkCtrl.Checked)
                {
                    string name = row.Cells[2].Text;
                    string code = row.Cells[3].Text;
                    string category = row.Cells[4].Text;
                    string Quantity = row.Cells[5].Text;
                    string costprice = row.Cells[6].Text;
                    string total = row.Cells[7].Text;
                    string user = row.Cells[8].Text;
                    string date = row.Cells[9].Text;
                    //string country = (row.Cells[2].FindControl("lblCountry") as Label).Text;
                    dt.Rows.Add(name, code, category, Quantity, costprice, total, user, date);
                }
            }
        }
        gridaccepteditems.DataSource = dt;
        gridaccepteditems.DataBind();
    }


    public void BindDataByDate()
    {
        string dateform = Convert.ToDateTime(txtdate.Text).ToString("yyyy-MM-dd");
        string date = "%" + dateform + "%";
        string query = "SELECT name, code, category, quantity, costprice, total, user FROM allocation  WHERE date LIKE '" + date + "'";
        string errormessage = "";
        try
        {
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(query, con))
                {
                    com.Parameters.AddWithValue("?date", date.ToString());
                    DataSet ds = new DataSet();
                    using(OdbcDataAdapter adapter = new OdbcDataAdapter())
                    {
                        adapter.SelectCommand = com;
                        adapter.Fill(ds);
                        Gridallocation.DataSource = ds.Tables[0];
                        Gridallocation.DataBind();

                    }

                }


            }
        }
        catch(Exception ex)
        {

            ShowMessage("Oops an error has occured", EnumMessages.MessageType.Error);
        }

    }

    public void SaveAcceptedAndUpdateStock()
    {
        string errormessage = "";
        try{
            using(OdbcConnection conn = DBConnection.getconnectionString(errormessage.ToString()))
            {
                conn.Open();

                foreach(GridViewRow row in gridaccepteditems.Rows)
                {
                    string cde;
                    Int64 qty;
                    Int64 stock2;
                    Int64 newstock;
                    string stock;
                     string updatestck;
                     string query = "INSERT INTO acceptedallocation(name,code,category,quantity,costprice, total,user, allocationdate) VALUES(?,?,?,?,?,?,?,?)";
                    //insertind sales into acceptedallocation table
                   using(OdbcCommand cmmd = new OdbcCommand(query, conn))
	                {
                        cmmd.Parameters.AddWithValue("?name", row.Cells[1].Text);
                        cmmd.Parameters.AddWithValue("?code", row.Cells[2].Text);
                        cmmd.Parameters.AddWithValue("?category", row.Cells[3].Text);
                        cmmd.Parameters.AddWithValue("?quantity", row.Cells[4].Text);
                        cmmd.Parameters.AddWithValue("?costprice", row.Cells[5].Text);
                        cmmd.Parameters.AddWithValue("?total", row.Cells[6].Text);
                        cmmd.Parameters.AddWithValue("?user", row.Cells[7].Text);
                        cmmd.Parameters.AddWithValue("?allocationdate", row.Cells[8].Text);
                        cmmd.ExecuteNonQuery();
                    
	                }
                        //updating stock

                         cde = row.Cells[2].Text;
                        //qty = Int64.Parse(rom.Cells[5].Text);
                        // getting the current stock a particular item code
                        string str = "Select id, code,category,  name, s_price,  c_price, stock, qtysold from items where code='" + cde + "'";
                        using(OdbcCommand sqcommand = new OdbcCommand(str, conn))
                        {
                            OdbcDataReader reader = sqcommand.ExecuteReader();
                            while(reader.Read())
                            {
                                stock = reader["stock"].ToString();
                                newstock = Int64.Parse(stock) + Int64.Parse((row.Cells[6]).Text);
                                updatestck = newstock.ToString();
                                string mquery = "update items set stock='" + updatestck + "' where code='" + cde + "';";
                                using(OdbcCommand command = new OdbcCommand(mquery, conn))
                                {
                                    command.ExecuteNonQuery();
                                }
                            }

                        }
                }
            }
        }
        catch(Exception ex)
        {

            ShowMessage("Oopps an has occured", EnumMessages.MessageType.Error);
        }

        BindAcceptedGrid();
    }
    public void BindData()
    {
        string errormessage = "";
        try
        {
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(DBqueries.getallocateditem, con))
                {
                    DataSet ds = new DataSet();
                    using(OdbcDataAdapter adapter = new OdbcDataAdapter())
                    {
                        adapter.SelectCommand = com;
                        adapter.Fill(ds);
                        Gridallocation.DataSource = ds.Tables[0];
                        Gridallocation.DataBind();

                    }

                }


            }
        }
        catch(Exception ex)
        {
            
            ShowMessage("Oops an error has occured", EnumMessages.MessageType.Error);
        }

    }

    protected void BindGrid()
    {
        gridaccepteditems.DataSource = ViewState["dt"] as DataTable;
        gridaccepteditems.DataBind();
    }

    protected void BindAcceptedGrid()
    {
        gridaccepteditems.DataSource = ViewState["dt"] as DataTable;
        gridaccepteditems.DataBind();
    }
    protected void btnsaveallocation_Click(object sender, EventArgs e)
    {
        SaveAcceptedAndUpdateStock();
    }
    protected void btncancelallocation_Click(object sender, EventArgs e)
    {

    }
    protected void btnaccept_Click(object sender, EventArgs e)
    {
        GetSelectedRecords();
    }
    protected void btnrejectallocation_Click(object sender, EventArgs e)
    {

    }
    protected void Gridallocation_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gridaccepteditems_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)");
            e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)");
        }
    }
    protected void Gridallocation_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gridaccepteditems_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int index = Convert.ToInt32(e.RowIndex);
        DataTable dt = ViewState["dt"] as DataTable;
        dt.Rows[index].Delete();
        ViewState["dt"] = dt;
        BindGrid();
    }
    protected void gridaccepteditems_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void btnloaddata_Click(object sender, EventArgs e)
    {
        BindDataByDate();
        BindGrid();
    }
}