﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Odbc;
using Microsoft.Reporting.WebForms;

public partial class itemsalesReportviewer : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        deletesalereport();
    }

    public void getSaleReport()
    {
        try
        {
             string dtfrom;
        string dtto;
        //string datee;
        dtfrom = Convert.ToDateTime(txtfromm.Text).ToString("yyyy-MM-dd");
        dtto = Convert.ToDateTime(txttto.Text).ToString("yyyy-MM-dd");
        string date = "%" + dtto + "%";
        string query = "select code, product, quantity, total from  rptitemsoldoffline WHERE date between'" + dtfrom + "' and '" + dtto + "' OR `date` LIKE '" + date + "'";
        string errormessage = "";
        using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
        {
            con.Open();
            using(OdbcCommand com = new OdbcCommand(query, con))
            {
                using(OdbcDataReader reader = com.ExecuteReader())
                {
                    string total = "";
                    string quantity = "";
                    string name = "";
                    string code = "";
                    while(reader.Read())
                    {
                        code = reader["code"].ToString();
                        name = reader["product"].ToString();
                        quantity = reader["quantity"].ToString();
                        total = reader["total"].ToString();


                        string str = "Select name, code, quantity, total from salesreport where code=?";
                        using(OdbcCommand sqcommand = new OdbcCommand(str, con))
                        {
                            sqcommand.Parameters.AddWithValue("?code", code);
                            string S_name = "";
                            string S_code = "";
                            string S_quantity = "";
                            string S_total = "";

                            OdbcDataReader reader1 = sqcommand.ExecuteReader();
                            Int32 U_total = 0;
                            Int32 U_quantity = 0;
                            if(reader1.HasRows)
                            {
                                while(reader1.Read())
                                {
                                    S_name = reader1["name"].ToString();
                                    S_code = reader1["code"].ToString();
                                    S_quantity = reader1["quantity"].ToString();
                                    S_total = reader1["total"].ToString();
                                   
                                    U_total = Int32.Parse(S_total) + Int32.Parse(total);
                                    U_quantity = Int32.Parse(S_quantity) + Int32.Parse(quantity);

                                    //int finaltotal = U_total;
                                    //finaltotal = finaltotal + U_total;

                                    //int finalquantity = U_quantity;
                                    //finalquantity = finalquantity + U_quantity;

                                    string updatetotal = U_total.ToString();
                                    string updatequantity = U_quantity.ToString();

                                    string mquery = "update salesreport set total ='" + updatetotal + "',  quantity ='" + updatequantity + "' where code='" + S_code + "'";

                                    using(OdbcCommand command = new OdbcCommand(mquery, con))
                                    {
                                        command.ExecuteNonQuery();
                                    }

                                }
                            }
                            else
                            {
                                using(OdbcCommand comm = new OdbcCommand(DBqueries.savesales, con))
                                {
                                    comm.Parameters.AddWithValue("?name", name);
                                    comm.Parameters.AddWithValue("?code", code);
                                    comm.Parameters.AddWithValue("?quantity", quantity);
                                    comm.Parameters.AddWithValue("?total", total);
                                    comm.ExecuteNonQuery();
                                }
                            }

                        }
                    }
                }
            }
        }
        }
        catch(Exception)
        {

            ShowMessage("OOPs an error has occured", EnumMessages.MessageType.Error);
        }
    }


    protected void ShowMessage(string Message, EnumMessages.MessageType type)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
    }
    public void deletesalereport()
    {
        string errormessage = "";
        using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
        {
            con.Open();
            using(OdbcCommand com = new OdbcCommand(DBqueries.deletesalesreport, con))
            {
                com.ExecuteNonQuery();
            }
        }      
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        getSaleReport();
        ReportViewer1.ProcessingMode = ProcessingMode.Local;
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Report.rdlc");
        //solditems saleitem = GetData("select * from  itemsold WHERE transdate between'" + dtfrom + "' and '" + dtto + "' OR `transdate` LIKE '" + date + "'");
        solditems saleitem = GetData("select * from  salesreport");
        ReportDataSource datasource = new ReportDataSource("DataSet1", saleitem.Tables[0]);
        ReportViewer1.LocalReport.DataSources.Clear();
        ReportViewer1.LocalReport.DataSources.Add(datasource);
    }

    private solditems GetData(string query)
    {
        try
        {
            string errormessage = "";
            OdbcCommand com = new OdbcCommand(query);
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcDataAdapter sda = new OdbcDataAdapter())
                {
                    com.Connection = con;
                    sda.SelectCommand = com;
                    using(solditems saleitem = new solditems())
                    {
                        sda.Fill(saleitem, "DataTable1");
                        return saleitem;
                    }
                }

            }
        }
        catch(Exception)
        {

            throw;
        }
    }
}