﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Odbc;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Net;

public partial class forgetpassword : System.Web.UI.Page

{
   
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ShowMessage(string Message, EnumMessages.MessageType type)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
    }
    protected void senMail()
    {
        string username = string.Empty;
        string password = string.Empty;
        try
        {
            using(OdbcConnection con = DBConnection.getconnectionString(lblMessage.Text))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(DBqueries.recoverpasswordquery,con))
                {
                    com.Parameters.AddWithValue("?email", txtemail.Text.Trim());
                    using( OdbcDataReader reader = com.ExecuteReader())
                    {
                        if(reader.Read())
                        {
                            username = reader["username"].ToString();
                            password = reader["password"].ToString();
                        }
                    }

                    com.ExecuteNonQuery();
                }
            }

            if(!string.IsNullOrEmpty(password))
            {
                MailMessage mm = new MailMessage("thepen0411@gmail.com", txtemail.Text.Trim());
                mm.Subject = "Password Recovery";
                mm.Body = string.Format("Hi {0},<br /><br />Your password is {1}.<br /><br />Thank You.", username, password);
                mm.IsBodyHtml = true;
               SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential();
                NetworkCred.UserName = "thepen0411@gmail.com";
                NetworkCred.Password = "fathia1234";
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
                lblMessage.ForeColor = Color.Green;
                ShowMessage("Password has been sent to your email address.", EnumMessages.MessageType.Success);
                //lblMessage.Text = "Password has been sent to your email address.";
            }
            else
            {
                //lblMessage.ForeColor = Color.Red;
                //lblMessage.Text = "This email address does not match our records.";
                ShowMessage("This email address does not match our records.", EnumMessages.MessageType.Info);
            }

        }
        catch(Exception)
        {
            //lblMessage.ForeColor = Color.Red;
            //lblMessage.Text = "Oops an error has occured";
            ShowMessage("Oops an error has occured.", EnumMessages.MessageType.Error);
           
        }
    }
    protected void btnrecoveremail_Click(object sender, EventArgs e)
    {
        senMail();
        txtemail.Text = "";
    }
}