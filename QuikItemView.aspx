﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="QuikItemView.aspx.cs" Inherits="QuikItemView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Quick Item View</title>
    <link href="Styles/LandingPage.css" rel="stylesheet" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="bootstrap/fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="Styles/styles.css" rel="stylesheet" />
    <link href="Styles/Pretty-Registration-Form.css" rel="stylesheet" />
    <link href="Content/bootstrap-theme.min.css" rel="stylesheet" />
    <script src="Scripts/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/alertmessages.js"></script>


    <script type="text/javascript">
        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            return false;
        }
    </script>
    <style>
        .GridPager a,
        .GridPager span {
            display: inline-block;
            padding: 0px 9px;
            margin-right: 4px;
            border-radius: 3px;
            border: solid 1px #c0c0c0;
            background: #e9e9e9;
            box-shadow: inset 0px 1px 0px rgba(255,255,255, .8), 0px 1px 3px rgba(0,0,0, .1);
            font-size: .875em;
            font-weight: bold;
            text-decoration: none;
            color: #717171;
            text-shadow: 0px 1px 0px rgba(255,255,255, 1);
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background: #616161;
            box-shadow: inset 0px 0px 8px rgba(0,0,0, .5), 0px 1px 0px rgba(255,255,255, .8);
            color: #f0f0f0;
            text-shadow: 0px 0px 3px rgba(0,0,0, .5);
            border: 1px solid #3AC0F2;
        }

        .messagealert {
            width: 30%;
            position: fixed;
            top: 0px;
            z-index: 100000;
            padding: 0;
            font-size: 15px;
            margin-right: 30%;
        }

        .dropdown-submenu {
            position: relative;
        }

            .dropdown-submenu .dropdown-menu {
                top: 0;
                left: 100%;
                margin-top: -1px;
            }
    </style>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span class=" fa fa-briefcase"></span>Inventory Application</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Manual</a></li>


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="itemsalesReportviewer.aspx"><span class="fa fa-file"></span>|Sales and Receipt</a></li>
                        <li><a href="stockReport.aspx"><span class="fa fa-file"></span>|Item Reports</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><span class="fa fa-file"></span>|Other Reports</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tools<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="mastermenu.aspx"><span class="glyphicon glyphicon-cog"></span>|Setting</a></li>
                        <li class="dropdown-submenu">
                            <a class="test" href="#"><span class="glyphicon glyphicon-refresh"></span>|Synchronization <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#"><span class="glyphicon glyphicon-upload"></span>|Upload</a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-download"></span>|Download</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span class="glyphicon glyphicon-th-list"></span>|ROL</a></li>
                        <li><a href="QuikItemView.aspx"><span class="glyphicon glyphicon-eye-open"></span>|Stock Preview</a></li>
                        <%--  <li><a href="#"><span class="glyphicon glyphicon-eye-open"></span>|Prouct Preview</a></li>--%>
                        <li class="divider"></li>
                        <li class="dropdown-submenu">
                            <a class="test" href="#"><span class="glyphicon glyphicon-tasks"></span>|Item Management<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="AddnewItem.aspx"><span class="glyphicon glyphicon-plus"></span>|Add Item</a></li>
                                <li><a href="QuikItemView.aspx"><span class="glyphicon glyphicon-plus"></span>|View Added Items</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-right">
                <div class="form-group">
                    <a href="login.aspx" class="btn btn-primary">Log Out</a>
                    <%--<input type="datetime" class="form-control" placeholder="Search"/>--%>
                </div>

            </form>
        </div>
    </nav>
    <div class="container">
        <div class="messagealert" id="alert_container">
        </div>
        <form id="form1" runat="server" role="form">
            <div id="modalPopUp" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">
                                <span id="spnTitle"></span>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                <span id="spnMsg"></span>.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="btnConfirm" class="btn btn-danger">
                                Yes, please</button>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Label runat="server"></asp:Label>
            <div class="row">
                <div class="col-md-10 col-xs-4" style="margin-top: 5%">
                    <div class="panel panel-primary">
                        <div class="panel panel-heading">
                            <h1 class="panel-title center-block">ITEM VIEW</h1>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive" style="height: 400%; width: 900%; overflow: auto; margin: 0 auto; text-align: center;">
                                <asp:GridView ID="gidviewitem" runat="server" BackColor="White" ShowHeaderWhenEmpty="True" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CssClass="table table-striped table-hover" CellPadding="3" CellSpacing="1" AllowPaging="true" AllowSorting="true" PageSize="10" AlternatingRowStyle-BackColor="#C2D69B" Width="722px" AutoGenerateColumns="False" OnPageIndexChanging="gidviewitem_PageIndexChanging" OnSelectedIndexChanged="gidviewitem_SelectedIndexChanged">
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="id" HeaderText="ID" SortExpression="code" />
                                        <asp:BoundField DataField="code" HeaderText="Code" SortExpression="code" />
                                        <%--<asp:BoundField DataField="Name" HeaderText="Product Name" SortExpression="ProductName" />--%>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Product Name:
                                            <asp:DropDownList ID="ddlName" runat="server"
                                                OnSelectedIndexChanged="ddlName_SelectedIndexChanged" AutoPostBack="true"
                                                AppendDataBoundItems="true">
                                            </asp:DropDownList>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("Name") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="category" HeaderText="Category" SortExpression="category" />
                                        <asp:BoundField DataField="s_price" HeaderText="Price" SortExpression="Price" />
                                        <asp:BoundField DataField="Stock" HeaderText="Stock" SortExpression="Stock" />
                                        <asp:BoundField DataField="ROL" HeaderText="ROL" SortExpression="Quantity" />

                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" CssClass="GridPager" />
                                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                                    <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#594B9C" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#33276A" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-xs-4" style="margin-top: 5%">
                    <div class="panel panel-primary">
                        <div class="panel panel-heading">
                            <h1 class="panel-title center-block">Quick Search</h1>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class=" col-md-2 col-xs-2">
                                    <label class="control-label" for="txtsearch">Search For:</label>
                                </div>
                                <div class="col-md-4 col-md-pull-0">
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtsearch" OnTextChanged="txtsearch_TextChanged" AutoPostBack="true"></asp:TextBox>
                                </div>
                                <div class="col-md-2">
                                    <label class="control-label" for="drdownlistsearchby">By:</label>
                                </div>
                                <div class="col-md-4 col-md-pull-2">
                                    <asp:DropDownList runat="server" ID="drdownlistsearchby" CssClass="form-control" OnSelectedIndexChanged="drdownlistsearchby_SelectedIndexChanged">
                                        <asp:ListItem Value="0" Text="Name"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Code"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="site-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Powered By Sekat Technologies © 2017</h5>
                        </div>
                        <div class="col-sm-6 social-icons"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter-square"></i></a><a href="#"><i class="fa fa-instagram"></i></a></div>
                    </div>
                </div>
            </footer>
        </form>
    </div>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/alertmessages.js"></script>

</body>
</html>
