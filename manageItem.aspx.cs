﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manageItem : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!this.IsPostBack)
        {
            //
            populateItemDropDownList();
        }
    }


    public void checkeditem()
    {
        if(rdbupdateitem.Checked == true)
        {
            btnchangeprice.Enabled = false;
            btndelete.Enabled = false;
        }
    }
    public void populateItemDropDownList()
    {
        string erromessage = "";
        try
        {
            using(OdbcConnection con = DBConnection.getconnectionString(erromessage))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(DBqueries.updatedropdwonlistquery, con))
                {
                    drpitemslist.DataSource = com.ExecuteReader();
                    drpitemslist.DataTextField = "id";
                    drpitemslist.DataTextField = "name";
                    drpitemslist.DataBind();
                    con.Close();
                }
            }
            drpitemslist.Items.Insert(0, new ListItem("--Select Product--", "0"));
        }
        catch(Exception ex)
        {
            erromessage = "An Error has Occured";
        }
    }

    public void updatestocktextbox()
    {
        string erroMessage = "";
        string code = "";
        string name = "";
        string category = "";
        string sellingPrice = "";
        string costPrice = "";
        string stock = "";
        try
        {
            string str = "Select id, code,category,  name, s_price,  c_price, stock, qtysold from items where" + " name =?";
            using(OdbcConnection con = DBConnection.getconnectionString(erroMessage))
            {
                con.Open();

                using(OdbcCommand vercommand = new OdbcCommand(str, con))
                {
                    vercommand.Parameters.AddWithValue("?name", drpitemslist.SelectedItem.Value);
                    OdbcDataReader reader = vercommand.ExecuteReader();

                    while(reader.Read())
                    {
                        code = reader["code"].ToString();
                        name = reader["name"].ToString();
                        category = reader["category"].ToString();
                        sellingPrice = reader["s_price"].ToString();
                        costPrice = reader["c_price"].ToString();
                        stock = reader["stock"].ToString();
                       
                    }
                    if(stock == null)
                    {
                        stock = "0";
                    }
                    //old in fromation panel
                    txtdisplaycode.Text = code;
                    txtdisplayname.Text = name;
                    txtdisplaycatefory.Text = category;
                    txtdisplaysellingprice.Text = sellingPrice;
                    txtdisplaycostprice.Text = costPrice;
                    txtdisplaystock.Text = stock;

                    //editing item panel
                    txteditname.Text = name;
                    txteditcategory.Text = category;
                    txtedtsellinprice.Text = sellingPrice;
                    txteditcosprice.Text = costPrice;
                }
            }
        }
        catch(Exception)
        {

            throw;
        }
    }
    protected void ShowMessage(string Message, EnumMessages.MessageType type)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
    }

    public void updateitem()
    {
        string errormessage = "";
        try
        {
           
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcCommand command = new OdbcCommand(DBqueries.updateitem, con))
                {
                    command.Parameters.Add("?name", OdbcType.VarChar).Value = txteditname;
                    command.Parameters.Add("?category", OdbcType.VarChar).Value = txteditcategory;
                    command.Parameters.Add("?s_price", OdbcType.VarChar).Value = txtedtsellinprice;
                    command.Parameters.Add("?c_price", OdbcType.VarChar).Value = txteditcosprice;
                    command.Parameters.Add("?name", OdbcType.VarChar).Value = txteditname;
                    command.Parameters.Add("?category", OdbcType.VarChar).Value = txteditcategory;
                    command.Parameters.Add("?id", OdbcType.Int).Value = drpitemslist.SelectedValue;
                    command.Parameters.Add("?code", OdbcType.VarChar).Value = txtdisplaycode;

                    int rowcount = command.ExecuteNonQuery();
                    if(rowcount > 0)
                    {
                        ShowMessage("Item has been Updated", EnumMessages.MessageType.Success); 
                    }
                }
                
            }
        }
        catch(Exception)
        {

            ShowMessage("Oops an error has occured", EnumMessages.MessageType.Error); 
        }

       
    }
    public void delteitem()
    {
        string errormessage = "";
        int rowaffected = 0;
        try
        {
            string mquery = "delete from items  where code='" + txtdisplaycode + "' AND code='" + drpitemslist.SelectedValue + "' ;";
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(mquery, con))
                {
                    rowaffected = com.ExecuteNonQuery();
                    if(rowaffected > 0 )
                    {
                        ShowMessage("Recorde has been deleted", EnumMessages.MessageType.Success);
                    }
                }
            }
        }
        catch(Exception)
        {

            ShowMessage("Oops an error has occured", EnumMessages.MessageType.Error);
        }
    }

protected void drpitemslist_SelectedIndexChanged(object sender, EventArgs e)
{
    updatestocktextbox();
}

protected void btndelete_Click(object sender, EventArgs e)
{
    delteitem();
}
protected void btnchangeprice_Click(object sender, EventArgs e)
{

}
protected void rdbdeletestock_CheckedChanged(object sender, EventArgs e)
{

}
protected void rdbchangeprice_CheckedChanged(object sender, EventArgs e)
{

}

protected void rdbnewproduct_CheckedChanged(object sender, EventArgs e)
{

}
protected void rdbupdateitem_CheckedChanged(object sender, EventArgs e)
{
    btnchangeprice.Enabled = false;
    btndelete.Enabled = false;
}
protected void btnaddnewItem_Click(object sender, EventArgs e)
{

}
protected void btncancelladnewitem_Click(object sender, EventArgs e)
{

}
protected void btnupdate_Click(object sender, EventArgs e)
{
    updateitem();
}
}