﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;


public partial class purchaseReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string dtfrom;
        string dtto;
        //string datee;

        dtfrom = Convert.ToDateTime(txtfrom.Text).ToString("yyyy-MM-dd");
        dtto = Convert.ToDateTime(txttto.Text).ToString("yyyy-MM-dd");

        string date = "%" + dtto + "%";
        ReportViewer1.ProcessingMode = ProcessingMode.Local;
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/PurchaseReport.rdlc");
        PurchaseItem purchaseitems = GetData("select * from  purchase WHERE date between'" + dtfrom + "' and '" + dtto + "' OR `date` LIKE '" + date + "'");
        ReportDataSource datasource = new ReportDataSource("DataSet1", purchaseitems.Tables[0]);
        ReportViewer1.LocalReport.DataSources.Clear();
        ReportViewer1.LocalReport.DataSources.Add(datasource);


    }

    private PurchaseItem GetData(string query)
    {
        try
        {
            string errormessage = "";
            OdbcCommand com = new OdbcCommand(query);
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcDataAdapter sda = new OdbcDataAdapter())
                {
                    com.Connection = con;
                    sda.SelectCommand = com;
                    using(PurchaseItem purchaseitems = new PurchaseItem())
                    {
                        sda.Fill(purchaseitems, "DataTable1");
                        return purchaseitems;
                    }
                }

            }
        }
        catch(Exception)
        {

            throw;
        }
    }
}