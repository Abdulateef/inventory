﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddnewItem : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        txtname.Focus();
        clearText();
        if(!IsPostBack)
        {
            populateItemDropDownList();
        }
    }

    //method to shoe popup message usng Jquery and boostrap
    protected void ShowMessage(string Message, EnumMessages.MessageType type)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
    }


    //method to add new item into the Items database
    public void addNewItem()
    {
        string errormessage = "";
        try
        {

            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcCommand command = new OdbcCommand(DBqueries.insertIntoItems, con))
                {
                    command.Parameters.Add("?name", OdbcType.VarChar).Value = txtname.Text;
                    command.Parameters.Add("?code", OdbcType.VarChar).Value = txtcode.Text;
                    command.Parameters.Add("?category", OdbcType.VarChar).Value = txtcategory.Text;
                    command.Parameters.Add("?s_price", OdbcType.VarChar).Value = txtsellingprice.Text;
                    command.Parameters.Add("?c_price", OdbcType.VarChar).Value = txtcostprice.Text;
                    command.Parameters.Add("?ROL", OdbcType.VarChar).Value = txtrol;
                    command.Parameters.Add("?status", OdbcType.VarChar).Value = txtstatus;                
                    int rowcount = command.ExecuteNonQuery();
                    if(rowcount > 0)
                    {
                        ShowMessage("Item has been Updated", EnumMessages.MessageType.Success);
                    }
                    else
                    {
                        ShowMessage("Item was not added, please make sure all in formaton are flled correctly", EnumMessages.MessageType.Error);
                    }
                }

            }
        }
        catch(Exception)
        {

            ShowMessage("Oops an error has occured", EnumMessages.MessageType.Error);
        }


    }

    public void removeItem()
    {
        string errormessage = "";
        try
        {

            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcCommand command = new OdbcCommand(DBqueries.deleteFromItems, con))
                {
                    command.Parameters.Add("?name", OdbcType.VarChar).Value = ddlitemdelete.SelectedItem.Text;
                    command.Parameters.Add("?code", OdbcType.VarChar).Value = txtcodedelete.Text;
                    command.Parameters.Add("?category", OdbcType.VarChar).Value = txtcategorydelete.Text;
                    command.Parameters.Add("?s_price", OdbcType.VarChar).Value = txtsellingpricedelete.Text;
                    command.Parameters.Add("?c_price", OdbcType.VarChar).Value = txtcostpricedelete.Text;
                    command.Parameters.Add("?ROL", OdbcType.VarChar).Value = txtroldelete.Text;
                    command.Parameters.Add("?status", OdbcType.VarChar).Value = txtstatusdelete.Text;
                    int rowcount = command.ExecuteNonQuery();
                    if(rowcount > 0)
                    {
                        ShowMessage("Item has been Deleted", EnumMessages.MessageType.Success);
                    }
                    else
                    {
                        ShowMessage("No Item was deleted", EnumMessages.MessageType.Error);
                    }
                }

            }
        }
        catch(Exception)
        {

            ShowMessage("Oops an error has occured", EnumMessages.MessageType.Error);
        }


    }

    public void populateItemDropDownList()
    {
        string erromessage = "";
        try
        {
            using(OdbcConnection con = DBConnection.getconnectionString(erromessage))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(DBqueries.updatedropdwonlistquery, con))
                {
                    ddlitemdelete.DataSource = com.ExecuteReader();
                    ddlitemdelete.DataTextField = "id";
                    ddlitemdelete.DataTextField = "name";
                    ddlitemdelete.DataBind();
                    con.Close();
                }
            }
            ddlitemdelete.Items.Insert(0, new ListItem("--Select Item--", "0"));
        }
        catch(Exception ex)
        {
            erromessage = "An Error has Occured";
        }
    }


    public void updatecodetextbox2()
    {
        string erroMessage = "";
        string code = "";
        string status = "";
        string ROL = "";
        string sellingprice = "";
        string costprice = "";
        string stock = "";
        string category = "";
        try
        {
            string str = "Select id, code,category,  name, s_price,  c_price, stock, status, ROL from items where" + " name =?";
            using(OdbcConnection con = DBConnection.getconnectionString(erroMessage))
            {
                con.Open();

                using(OdbcCommand vercommand = new OdbcCommand(str, con))
                {
                    vercommand.Parameters.AddWithValue("?name", ddlitemdelete.SelectedItem.Value);
                    OdbcDataReader reader = vercommand.ExecuteReader();

                    while(reader.Read())
                    {
                        code = reader["code"].ToString();
                        category = reader["category"].ToString();
                        sellingprice = reader["s_price"].ToString();
                        costprice = reader["c_price"].ToString();
                        stock = reader["stock"].ToString();
                        status = reader["status"].ToString();
                        ROL = reader["ROL"].ToString();
                    }

                    //update code textbox
                    txtcodedelete.Text = code;
                    txtcategorydelete.Text = category;
                    txtcodedelete.Text = costprice;
                    txtsellingpricedelete.Text = sellingprice;
                    txtstatusdelete.Text = status;
                    txtstatusdelete.Text = stock;
                    txtroldelete.Text = ROL;


                }
            }
        }
        catch(Exception)
        {

            throw;
        }
    }

    //method to clear all textbox controls
    public void clearText()
    {
        txtstatus.Text = "";
        txtsellingprice.Text = "";
        txtrol.Text = "";
        txtname.Text = "";
        txtcostprice.Text = "";
        txtcategory.Text = "";
        txtcode.Text = "";
    }

    public void clearTextDelete()
    {
        txtstatusdelete.Text = "";
        txtsellingpricedelete.Text = "";
        txtcodedelete.Text = "";
        ddlitemdelete.SelectedItem.Text = "";
        txtcostpricedelete.Text = "";
        txtcategorydelete.Text = "";
        txtroldelete.Text = "";

    }
    protected void btnaddnewItem_Click(object sender, EventArgs e)
    {
        addNewItem();
        clearText();
    }
    protected void btncancelladnewitem_Click(object sender, EventArgs e)
    {
        clearText();
    }
    protected void ddlitemdelete_SelectedIndexChanged(object sender, EventArgs e)
    {
        updatecodetextbox2();
    }
    protected void btndlete_Click(object sender, EventArgs e)
    {
        removeItem();
    }
    protected void btncanceledelete_Click(object sender, EventArgs e)
    {
        clearTextDelete();
    }
}