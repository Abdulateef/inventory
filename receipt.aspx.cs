﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Data.Odbc;

public partial class receipt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!this.IsPostBack)
        {
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Receipt.rdlc");
            Receipt Receipt = GetData("select Name, Quantity, Price, Total FROM receipt");
            ReportDataSource datasource = new ReportDataSource("DataSet1", Receipt.Tables[0]);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(datasource);

            ReportParameterCollection reportparameter = new ReportParameterCollection();
            reportparameter.Add(new ReportParameter("productType", Globalvariables.username.ToString()));
            reportparameter.Add(new ReportParameter("customername", Globalvariables.username.ToString()));
            reportparameter.Add(new ReportParameter("customeraddress", Globalvariables.username.ToString()));
            this.ReportViewer1.LocalReport.SetParameters(reportparameter);
            this.ReportViewer1.LocalReport.Refresh();
        }

    }

    private Receipt GetData(string query)
    {
        try
        {
            string errormessage = "";
            OdbcCommand com = new OdbcCommand(query);
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcDataAdapter sda = new OdbcDataAdapter())
                {
                    com.Connection = con;
                    sda.SelectCommand = com;
                    using(Receipt Receipt = new Receipt())
                    {
                        sda.Fill(Receipt, "DataTable1");
                        return Receipt;
                    }
                }

            }
        }
        catch(Exception)
        {

            throw;
        }
    }
    protected void Unnamed_Click(object sender, EventArgs e)
    {

    }
    protected void btnshow_Click(object sender, EventArgs e)
    {
        ReportViewer1.ProcessingMode = ProcessingMode.Local;
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Receipt.rdlc");
        Receipt Receipt = GetData("select * from receipt");
        ReportDataSource datasource = new ReportDataSource("DataSet1", Receipt.Tables[0]);
        ReportViewer1.LocalReport.DataSources.Clear();
        ReportViewer1.LocalReport.DataSources.Add(datasource);

        //ReportParameterCollection reportparameter = new ReportParameterCollection();
        //reportparameter.Add(new ReportParameter("purchaseType", "Testing"));
        //this.ReportViewer1.LocalReport.SetParameters(reportparameter);
        //this.ReportViewer1.LocalReport.Refresh();
    }
}