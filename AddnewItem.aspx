﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddnewItem.aspx.cs" Inherits="AddnewItem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manage Item</title>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="bootstrap/fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="Styles/styles.css" rel="stylesheet" />
    <link href="Styles/LandingPage.css" rel="stylesheet" />
    <script src="Scripts/alertmessages.js"></script>

    <script type="text/javascript">
        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            return false;
        }
    </script>
    <style>
        hr.style5 {
            background-color: #fff;
            border-top: 2px dashed #8c8b8b;
        }

        .messagealert {
            width: 30%;
            position: fixed;
            top: 0px;
            z-index: 100000;
            padding: 0;
            font-size: 15px;
            margin-right: 30%;
        }


        .dropdown-submenu {
            position: relative;
        }

            .dropdown-submenu .dropdown-menu {
                top: 0;
                left: 100%;
                margin-top: -1px;
            }
    </style>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span class=" fa fa-briefcase"></span>Inventory Application</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Manual</a></li>


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="itemsalesReportviewer.aspx"><span class="fa fa-file"></span>|Sales and Receipt</a></li>
                        <li><a href="stockReport.aspx"><span class="fa fa-file"></span>|Item Reports</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><span class="fa fa-file"></span>|Other Reports</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tools<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="mastermenu.aspx"><span class="glyphicon glyphicon-cog"></span>|Setting</a></li>
                        <li class="dropdown-submenu">
                            <a class="test" href="#"><span class="glyphicon glyphicon-refresh"></span>|Synchronization <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#"><span class="glyphicon glyphicon-upload"></span>|Upload</a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-download"></span>|Download</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span class="glyphicon glyphicon-th-list"></span>|ROL</a></li>
                        <li><a href="QuikItemView.aspx"><span class="glyphicon glyphicon-eye-open"></span>|Stock Preview</a></li>
                        <%--  <li><a href="#"><span class="glyphicon glyphicon-eye-open"></span>|Prouct Preview</a></li>--%>
                        <li class="divider"></li>
                        <li class="dropdown-submenu">
                            <a class="test" href="#"><span class="glyphicon glyphicon-tasks"></span>|Item Management<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="AddnewItem.aspx"><span class="glyphicon glyphicon-plus"></span>|Add Item</a></li>
                                <li><a href="QuikItemView.aspx"><span class="glyphicon glyphicon-plus"></span>|View Added Items</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-right">
                <div class="form-group">
                    <a href="login.aspx" class="btn btn-primary">Log Out</a>
                    <%--<input type="datetime" class="form-control" placeholder="Search"/>--%>
                </div>

            </form>
        </div>
    </nav>
    <br />
    <br />
    <br />
    <%-- Content --%>
    <div class="container">
        <form id="form1" runat="server" class="form-horizontal">
            <div class="messagealert" id="alert_container">
            </div>
            <div id="modalPopUp" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">
                                <span id="spnTitle"></span>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                <span id="spnMsg"></span>.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="btnConfirm" class="btn btn-danger">
                                Yes, please</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-6 col-xs-4 col-md-offset-3">
                    <div class="panel panel-primary ">
                        <div class="panel-heading">Add Item</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-4 control-label">
                                    <label class="control-label" for="txtname">Name</label>
                                </div>
                                <div class="col-sm-6 input-column ">
                                    <asp:TextBox runat="server" ID="txtname" required="true" CssClass="form-control" placeholder="Enter Prodcute Name" ></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-4 control-label">
                                    <label class="control-label" for="txtcategory">Category</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtcategory" required="true" CssClass="form-control" Placeholder="Enter Category"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 control-label">
                                    <label class="control-label" for="txtsellingprice">Selling Price</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtsellingprice" required="true" CssClass="form-control"  Placeholder="Selling Price" TextMode="Number"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 control-label">
                                    <label class="control-label" for="txtcostprice">Cost Price</label>
                                </div>
                                <div class="col-sm-6  input-column">
                                    <asp:TextBox runat="server" ID="txtcostprice" required="true" CssClass="form-control" Placeholder="Enter Cost Price" TextMode="Number"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 control-label">
                                    <label class="control-label" for="txtrol">ROL</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtrol"  CssClass="form-control" required="true" Placeholder="Enter ROL" TextMode="Number"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 col-xs-4 control-label">
                                    <label class="control-label" for="txtcode">Code</label>
                                </div>
                                <div class="col-sm-6  input-column">
                                    <asp:TextBox runat="server" ID="txtcode" required="true" CssClass="form-control" Placeholder="Enter Code"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 control-label">
                                    <label class="control-label" for="txtstatus">Status</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtstatus" required="true" CssClass="form-control" Placeholder="Ente Status"></asp:TextBox>
                                </div>
                            </div>


                            <div class="row col-md-6 col-xs-4 col-md-offset-4">

                                <div class="col-md-3 col-xs-3 col-md-offset-2">
                                    <asp:LinkButton ID="btnaddnewItem" runat="server" CssClass="btn btn-success" OnClientClick="return getConfirmation(this, 'Please confirm','Are you sure you want to add Item?');" OnClick="btnaddnewItem_Click"><i class="glyphicon glyphicon-upload"></i>&nbsp;Add</asp:LinkButton>
                                </div>

                                <div class=" col-md-3 col-xs-3 col-md-offset-1">
                                    <asp:LinkButton ID="btncancelladnewitem" runat="server" CssClass="btn btn-danger" OnClick="btncancelladnewitem_Click"><i class="glyphicon glyphicon-erase"></i>&nbsp;Cancel</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="style5" />
            <div class="row ">
                <div class="col-md-6 col-xs-4 col-md-offset-3">
                    <div class="panel panel-primary ">
                        <div class="panel-heading">Remove Item</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-4 control-label">
                                    <label class="control-label" for="ddlitemdelete">Name</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:DropDownList runat="server" ID="ddlitemdelete" CssClass="form-control" OnSelectedIndexChanged="ddlitemdelete_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-4 control-label">
                                    <label class="control-label" for="txtcategorydelete">Category</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtcategorydelete" Enabled="false" CssClass="form-control" required="true" Placeholder="Enter Category"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 control-label">
                                    <label class="control-label" for="txtsellingpricedelete">Selling Price</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtsellingpricedelete" Enabled="false" CssClass="form-control" required="true" Placeholder="Selling Price"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 control-label">
                                    <label class="control-label" for="txtcostpricedelete">Cost Price</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtcostpricedelete" Enabled="false" CssClass="form-control" required="true" Placeholder="Enter Cost Price"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 control-label">
                                    <label class="control-label" for="txtroldelete">ROL</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtroldelete" Enabled="false" CssClass="form-control" required="true" Placeholder="Enter ROL"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 control-label">
                                    <label class="control-label" for="txtcodedelete">Code</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtcodedelete" Enabled="false" CssClass="form-control" required="true" Placeholder="Enter Code"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 control-label">
                                    <label class="control-label" for="txtstatusdelete">Status</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtstatusdelete" Enabled="false" CssClass="form-control" required="true" Placeholder="Ente Status"></asp:TextBox>
                                </div>
                            </div>

                            <div class="row col-md-6 col-xs-4 col-md-offset-4">

                                <div class="col-md-3 col-xs-3 col-md-offset-2">
                                    <asp:LinkButton ID="btndlete" runat="server" CssClass="btn btn-success" OnClientClick="return getConfirmation(this, 'Please confirm','Are you sure you want to Delete Item?');" OnClick="btndlete_Click"><i class="glyphicon glyphicon-trash"></i>&nbsp;Delete</asp:LinkButton>
                                </div>

                                <div class=" col-md-3 col-xs-3 col-md-offset-2">
                                    <asp:LinkButton ID="btncanceledelete" runat="server" CssClass="btn btn-danger" OnClick="btncanceledelete_Click"><i class="glyphicon glyphicon-erase"></i>&nbsp;Cancel</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="site-footer" style="margin-top: 10%">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Powered By Sekat Technologies © 2017</h5>
                        </div>
                        <div class="col-sm-6 social-icons"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter-square"></i></a><a href="#"><i class="fa fa-instagram"></i></a></div>
                    </div>
                </div>
            </footer>
        </form>
    </div>
    <script src="Scripts/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.dropdown-submenu a.test').on("click", function (e) {
                $(this).next('ul').toggle();
                e.stopPropagation();
                e.preventDefault();
            });
        });
    </script>
</body>
</html>
