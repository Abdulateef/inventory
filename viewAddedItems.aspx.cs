﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Odbc;
using System.Data;

public partial class viewAddedItems : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            populateGridview();
        }
    }

    public void populateGridview()
    {
        string errormessage = "";
        try
        {
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcCommand comm = new OdbcCommand(DBqueries.updateaddeditemquery,con))
                {
                    using(OdbcDataAdapter adapter = new OdbcDataAdapter())
                    {
                        adapter.SelectCommand = comm;
                        using(DataSet dt = new DataSet())
                        {
                            adapter.Fill(dt);
                            GridProduct.DataSource = dt.Tables[0];
                            GridProduct.DataBind();
                        }
                    }
                }
            }
        }
        catch(Exception)
        {
            
            throw;
        }
    }
    protected void GridProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridProduct.PageIndex = e.NewPageIndex;
        GridProduct.DataBind();
    }
}

