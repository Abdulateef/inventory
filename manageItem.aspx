﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="manageItem.aspx.cs" Inherits="manageItem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manage Item</title>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="Styles/styles.css" rel="stylesheet" />
    <link href="Styles/Pretty-Registration-Form.css" rel="stylesheet" />

    <script src="Scripts/alertmessages.js"></script>

    <script type="text/javascript">
        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            return false;
        }
    </script>
    <style>
        .messagealert {
            width: 30%;
            position: fixed;
            top: 0px;
            z-index: 100000;
            padding: 0;
            font-size: 15px;
            margin-right: 30%;
        }


        .dropdown-submenu {
            position: relative;
        }

            .dropdown-submenu .dropdown-menu {
                top: 0;
                left: 100%;
                margin-top: -1px;
            }
    </style>
</head>
<body>
    <%-- Navbar --%>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span class=" fa fa-briefcase"></span>Inventory Application</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Manual</a></li>


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="itemsalesReportviewer.aspx"><span class="fa fa-file"></span>|Sales and Receipt</a></li>
                        <li><a href="stockReport.aspx"><span class="fa fa-file"></span>|Item Reports</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><span class="fa fa-file"></span>|Other Reports</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tools<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="mastermenu.aspx"><span class="glyphicon glyphicon-cog"></span>|Setting</a></li>
                        <li class="dropdown-submenu">
                            <a class="test" href="#"><span class="glyphicon glyphicon-refresh"></span>|Synchronization <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#"><span class="glyphicon glyphicon-upload"></span>|Upload</a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-download"></span>|Download</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span class="glyphicon glyphicon-th-list"></span>|ROL</a></li>
                        <li><a href="QuikItemView.aspx"><span class="glyphicon glyphicon-eye-open"></span>|Stock Preview</a></li>
                        <%--  <li><a href="#"><span class="glyphicon glyphicon-eye-open"></span>|Prouct Preview</a></li>--%>
                        <li class="divider"></li>
                        <li class="dropdown-submenu">
                            <a class="test" href="#"><span class="glyphicon glyphicon-tasks"></span>|Item Management<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="AddnewItem.aspx"><span class="glyphicon glyphicon-plus"></span>|Add Item</a></li>
                                <li><a href="QuikItemView.aspx"><span class="glyphicon glyphicon-plus"></span>|View Added Items</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-right">
                <div class="form-group">
                    <a href="login.aspx" class="btn btn-primary">Log Out</a>
                    <%--<input type="datetime" class="form-control" placeholder="Search"/>--%>
                </div>

            </form>
        </div>
    </nav>
    <br />
    <br />
    <br />
    <%-- Content Area --%>
    <div class="container">
        <form id="form1" runat="server" class="form-horizontal">
            <div class="messagealert" id="alert_container">
            </div>
            <div id="modalPopUp" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">
                                <span id="spnTitle"></span>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                <span id="spnMsg"></span>.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="btnConfirm" class="btn btn-danger">
                                Yes, please</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Perform an Action</div>
                        <div class="panel-body">
                            <div class="row">
                                <div>
                                    <div class="col-sm-offset-3 col-sm-10">
                                        <div class="radio ">
                                            <label class="radio-inline">
                                                <asp:RadioButton runat="server" ID="rdbupdateitem" Checked="true" GroupName="trasnsaction" Text="Upadate Item" OnCheckedChanged="rdbupdateitem_CheckedChanged" />
                                            </label>
                                            <label class="radio-inline">
                                                <asp:RadioButton runat="server" ID="rdbchangeprice" GroupName="trasnsaction" Text="Change Price" OnCheckedChanged="rdbchangeprice_CheckedChanged" />
                                            </label>
                                            <label class="radio-inline">
                                                <asp:RadioButton runat="server" ID="rdbdeletestock" GroupName="trasnsaction" Text="Delete Stock" OnCheckedChanged="rdbdeletestock_CheckedChanged" />
                                            </label>
                                            <label class="radio-inline">
                                                <asp:RadioButton runat="server" ID="rdbnewproduct" GroupName="trasnsaction" Text="New Product" OnCheckedChanged="rdbnewproduct_CheckedChanged" />
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sx-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Old Information</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-6 label-column">
                                    <asp:DropDownList runat="server" ID="drpitemslist" CssClass="form-control" OnSelectedIndexChanged="drpitemslist_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-4 input-column col-md-offset-1">
                                    <asp:TextBox runat="server" ID="txtdisplaycode" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 label-column">
                                    <label class="control-label" for="txtdisplayname">Name</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtdisplayname" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-4 label-column">
                                    <label class="control-label" for="txtdisplaycatefory">Category</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtdisplaycatefory" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                                <%--<div class="col-sm-3 input-column pull-right">
                            <asp:Button runat="server" class="btn btn-primary" Text="Add" ID="btnAdd" OnClick="btnAdd_Click" />
                        </div>--%>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 label-column">
                                    <label class="control-label" for="txtdisplaysellingprice">Selling Price</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtdisplaysellingprice" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 label-column">
                                    <label class="control-label" for="txtStock">Cost Price</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtdisplaycostprice" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 label-column">
                                    <label class="control-label" for="txtdisplaystock">Stock</label>
                                </div>
                                <div class="col-sm-2 input-column">
                                    <asp:TextBox runat="server" ID="txtdisplaystock" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vl"></div>
                <div class="col-md-6 col-xs-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">New Information</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-4 label-column">
                                    <label class="control-label" for="txteditname">Name</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txteditname" CssClass="form-control" required="true" placeholder="Enter Prodcute Name"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-4 label-column">
                                    <label class="control-label" for="txteditcategory">Category</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txteditcategory" CssClass="form-control" required="true" Placeholder="Enter Category"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 label-column">
                                    <label class="control-label" for="txtedtsellinprice">Selling Price</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txtedtsellinprice" CssClass="form-control" required="true" Placeholder="Selling Price"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 label-column">
                                    <label class="control-label" for="txteditcosprice">Cost Price</label>
                                </div>
                                <div class="col-sm-6 input-column">
                                    <asp:TextBox runat="server" ID="txteditcosprice" CssClass="form-control" required="true" Placeholder="Ente Price"></asp:TextBox>
                                </div>
                            </div>

                            <div class="row col-md-8 col-md-offset-3">

                                <div class="col-md-3 col-xs-3 col-md-offset-0">
                                    <asp:LinkButton ID="btnupdate" runat="server" CssClass="btn btn-success" OnClientClick="return getConfirmation(this, 'Please confirm','Are you sure you want to add Item?');" OnClick="btnupdate_Click"><i class="glyphicon glyphicon-upload"></i>&nbsp;Update</asp:LinkButton>
                                </div>

                                <div class=" col-md-3 col-xs-3 col-md-offset-1">
                                    <asp:LinkButton ID="btnchangeprice" runat="server" CssClass="btn btn-primary" OnClientClick="return getConfirmation(this, 'Please confirm','Are you sure you want to Change Item?');" OnClick="btnchangeprice_Click"><i class="glyphicon glyphicon-send"></i>&nbsp;Change</asp:LinkButton>
                                </div>

                                <div class=" col-md-3 col-xs-3 col-md-offset-1">
                                    <asp:LinkButton ID="btndelete" runat="server" CssClass="btn btn-danger" OnClientClick="return getConfirmation(this, 'Please confirm','Are you sure you want to Delete Item?');" OnClick="btndelete_Click"><i class="glyphicon glyphicon-trash"></i>&nbsp;Delete</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>

    <script src="Scripts/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.dropdown-submenu a.test').on("click", function (e) {
                $(this).next('ul').toggle();
                e.stopPropagation();
                e.preventDefault();
            });
        });
    </script>
</body>
</html>
