﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manageusers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!this.IsPostBack)
        {
            this.populateGridview();
        }
    }

    public void populateGridview()
    {
        string errormessage = "";
        try
        {
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcCommand comm = new OdbcCommand( DBqueries.selectusersquery, con))
                {
                    using(OdbcDataAdapter adapter = new OdbcDataAdapter())
                    {
                        adapter.SelectCommand = comm;
                        using(DataSet dt = new DataSet())
                        {
                            adapter.Fill(dt);
                            GridProduct.DataSource = dt;
                            GridProduct.DataBind();
                        }
                    }
                }
            }
        }
        catch(Exception)
        {

            throw;
        }
    }


    private void insert()
    {
        string errormessage = "";
        string username = txtusername.Text;
        string password = txtpassword.Text;
        string email = txtemail.Text;
        string masteruser = txtmasteruser.Text;
        string fullname = txtfullname.Text;
        Int32 index = GridProduct.Rows.Count + 1;

        try
        {
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcCommand com = new  OdbcCommand(DBqueries.insertnewuser, con))
                {
                    com.Parameters.AddWithValue("?rownumber", index);
                    com.Parameters.AddWithValue("?username", username.ToString());
                    com.Parameters.AddWithValue("?password", password.ToString());
                    com.Parameters.AddWithValue("fullname", fullname.ToString());
                    com.Parameters.AddWithValue("email", email.ToString());
                    com.Parameters.AddWithValue("masteruser", masteruser.ToString());
                    com.ExecuteNonQuery();
                }
            }

            this.populateGridview();
        }
        catch(Exception)
        {
            
            throw;
        }
    }
    protected void GridProduct_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Convert.ToInt32(GridProduct.DataKeys[e.RowIndex].Values[0]);
        Int32 index = GridProduct.Rows.Count + 1;
        try
        {
            string errormessage = "";
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(DBqueries.deleteuser, con))
                {
                    com.Parameters.AddWithValue("?rownumber", index);
                    com.ExecuteNonQuery();
                }
            }
            this.populateGridview();
        }
        catch(Exception)
        {

            throw;
        }
    }
    protected void GridProduct_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GridProduct.Rows[e.RowIndex];
        int id = Convert.ToInt32(GridProduct.DataKeys[e.RowIndex].Values[0]);
        Double index = GridProduct.Rows.Count ;
        string username = (row.FindControl("txtName") as TextBox).Text;
        string password = (row.FindControl("txtPassword") as TextBox).Text;
        string fullname = (row.FindControl("txtFullname") as TextBox).Text;
        string email = (row.FindControl("txtemail") as TextBox).Text;
        string masteruser = (row.FindControl("txtmasteruser") as TextBox).Text;

        GridProduct.Rows[e.RowIndex].Cells[1].Text = username.ToString();
        GridProduct.Rows[e.RowIndex].Cells[2].Text = password.ToString();
        GridProduct.Rows[e.RowIndex].Cells[3].Text = fullname.ToString();
        GridProduct.Rows[e.RowIndex].Cells[4].Text = email.ToString();
        GridProduct.Rows[e.RowIndex].Cells[5].Text = masteruser.ToString();
        try
        {
            string errormessage = "";
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(DBqueries.updateuser,con))
                {
                    com.Parameters.AddWithValue("?username", username.ToString());
                    com.Parameters.AddWithValue("?password", password.ToString());
                    com.Parameters.AddWithValue("?fullname", fullname.ToString());
                    com.Parameters.AddWithValue("?email", email.ToString());
                    com.Parameters.AddWithValue("?masteruser", masteruser.ToString());
                    com.Parameters.AddWithValue("?id", id);
                    com.ExecuteNonQuery();
                    GridProduct.EditIndex = -1;
                    this.populateGridview();
                }
            }
        }
        catch(Exception)
        {
            
            throw;
        }

        
     
    }
    protected void GridProduct_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != GridProduct.EditIndex)
        {
          (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
        }
    }
    protected void GridProduct_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridProduct.EditIndex = e.NewEditIndex;
        this.populateGridview();
    }
    protected void GridProduct_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridProduct.EditIndex = -1;
        this.populateGridview();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
       insert();





       //try
       //{
       //    int id = 2;
       //    string username = "lawal";
       //    string password = "1245";
       //    string fullname = "lawal lawal";
       //    string email = "thepen@gmail.com";
       //    string masteruser = "YES";


       //    string errormessage = "";
       //    using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
       //    {
       //        con.Open();
       //        using(OdbcCommand com = new OdbcCommand(DBqueries.updateuser, con))
       //        {
       //            com.Parameters.AddWithValue("?username", username.ToString());
       //            com.Parameters.AddWithValue("?password", password.ToString());
       //            com.Parameters.AddWithValue("?fullname", fullname.ToString());
       //            com.Parameters.AddWithValue("?email", email.ToString());
       //            com.Parameters.AddWithValue("?masteruser", masteruser.ToString());
       //            com.Parameters.AddWithValue("?id", id);
       //            com.ExecuteNonQuery();
       //        }
       //    }
       //}
       //catch(Exception)
       //{

       //    throw;
       //}
    }
    protected void GridProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
}