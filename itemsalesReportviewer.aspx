﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="itemsalesReportviewer.aspx.cs" Inherits="itemsalesReportviewer" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>





<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sales Report</title>
    <script src="Scripts/jquery-1.4.1.min.js"></script>
    <script src="Scripts/jquery.dynDateTime.min.js"></script>
      <script src="Scripts/calendar-en.min.js"></script>
     <link href="Styles/calendar-blue.css" rel="stylesheet" />
    <script src="Scripts/bootstrap.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <style>
        hr.style5 {
            background-color: #fff;
            border-top: 2px dashed #8c8b8b;
        }

         .messagealert {
            width: 30%;
            position: fixed;
            top: 0px;
            z-index: 100000;
            padding: 0;
            font-size: 15px;
            margin-right: 30%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="messagealert" id="alert_container">
        </div>
        <div class="row" style="margin-top: 2%">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">Select Date Range</div>
                    <div class="panel-body">
                        <div class="container">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-3 col-md-3">
                                        <asp:TextBox ID="txtfromm" runat="server" ReadOnly="false" placeholder="FROM" TextMode="Date" CssClass="form-control Calender" />
                                        <div class="col-xs-1 col-md-1">
                                            <img src="calender.png" />
                                        </div>
                                    </div>
                                    <div class="col-xs-3 col-md-3">
                                        <asp:TextBox ID="txttto" runat="server" ReadOnly="false" placeholder="TO" TextMode="Date" CssClass="Calender2 form-control" />
                                        <div class="col-xs-1 col-md-1">
                                            <img src="calender.png" />
                                        </div>
                                    </div>
                                    <div class="col-xs-2 col-md-2">
                                        <asp:Button ID="btnSearch" runat="server" Text="PREVIEW" OnClick="btnSearch_Click" CssClass="btn btn-sm btn-primary" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>               
            </div> 
              
        </div>
      
        <div class="container">
              <hr class="style5" />
            <div class="row">
                <div class="col-xs-6 col-md-6 col-md-offset-3">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="700px" Width="400px"></rsweb:ReportViewer>
                </div>
                
            </div>
        </div>

    </form>
</body>
</html>
