﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="rejecteditemReport.aspx.cs" Inherits="rejecteditemReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/jquery-1.4.1.min.js"></script>
    <script src="Scripts/jquery.dynDateTime.min.js"></script>
    <script src="Scripts/calendar-en.min.js"></script>
    <link href="Styles/calendar-blue.css" rel="stylesheet" />
    <script src="Scripts/bootstrap.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <style>
        hr.style5 {
            background-color: #fff;
            border-top: 2px dashed #8c8b8b;
        }

        .Calender2 {
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row" style="margin-top: 2%">
                <div class="col-md-10 col-md-push-2">
                    <div class="panel panel-primary">
                        <div class="panel-heading">&nbsp;Select Date Range</div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-1 label-column">
                                            <label class="control-label" for="txtfrom">From</label>
                                        </div>
                                        <div class="col-xs-3 col-md-3">
                                            <asp:TextBox runat="server" ID="txtfrom" TextMode="Date" CssClass="Calender form-control" placeholder="FROM"></asp:TextBox>
                                        </div>
                                        <div class="col-md-1 label-column">
                                            <label class="control-label" for="txttto">To</label>
                                        </div>
                                        <div class="col-xs-3 col-md-3">
                                            <asp:TextBox runat="server" ID="txttto" TextMode="Date" CssClass="Calender2 form-control" placeholder="To"></asp:TextBox>
                                        </div>

                                        <div class="col-xs-2 col-md-2">
                                            <asp:Button ID="btnSearch" runat="server" Text="PREVIEW" OnClick="btnSearch_Click" CssClass="btn btn-sm btn-primary" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="style5" />
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 col-md-6 col-md-offset-3">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="750px" Height="800px"></rsweb:ReportViewer>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="Scripts/jquery-1.9.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
</body>
</html>
