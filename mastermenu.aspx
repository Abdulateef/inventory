﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="mastermenu.aspx.cs" Inherits="mastermenu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Main Menu</title>
    <script src="Scripts/jquery-1.9.1.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="Styles/mainmenustyle.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <h3>INVENTORY APP</h3>
                    <strong>MI</strong>
                </div>

                <ul class="list-unstyled components">
                    <li class="active">
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">
                            <i class="glyphicon glyphicon-briefcase"></i>
                            <%-- Home--%>
                            Item Management
                        </a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li><a href="AddnewItem.aspx">Add New Item</a></li>
                            <li><a href="manageItem.aspx">Manage Items</a></li>
                            <li><a href="stockReport.aspx">Preview Available Items</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="About.aspx">
                            <i class="glyphicon glyphicon-edit"></i>
                            Manage Items
                        </a>
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">
                            <i class="glyphicon glyphicon-user"></i>
                            <%-- Pages--%>
                           Manage  Users
                        </a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li><a href="registrationpage.aspx">Register new User</a></li>
                            <li><a href="manageusers.aspx">Manage Users Records</a></li>
                            <li><a href="forgetpassword.aspx">Retrieve Password</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="Addpurchase.aspx">
                            <i class="glyphicon glyphicon-shopping-cart"></i>
                            Manage Purchases
                        </a>
                    </li>
                   <%-- <li>
                        <a href="client.aspx">
                            <i class="glyphicon glyphicon-user"></i>
                            New Clients
                        </a>
                    </li>--%>
                    <%--<li>
                        <a href="#">
                            <i class="glyphicon glyphicon-send"></i>
                            Contact
                        </a>
                    </li>--%>
                </ul>

                <ul class="list-unstyled CTAs">
                    <li><a href="https://bootstrapious.com/p/bootstrap-sidebar" class="article">View Reports</a></li>
                </ul>
            </nav>

            <!-- Page Content Holder -->
            <div id="content">

                <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-left"></i>
                                <span></span>Open Menu
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="Default.aspx">HOME</a></li>
                                <li><a href="SignIn.aspx">LOG OUT</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <h2>WELCOME</h2>
                <p>This System is design as an online real time inventory application system with friendly and attractive user interface and user experience .</p>
                <p>The new System is design to carryout some of the basic operations of managing your store any where any time.</p>
                <div class="line"></div>
            </div>
        </div>

    </form>

    <!-- jQuery CDN -->
    <script src="Scripts/jquery-1.8.3.js"></script>
    <!-- Bootstrap Js CDN -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>
</html>
