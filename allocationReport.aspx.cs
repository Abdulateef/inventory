﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class allocationReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }



    private allocation GetData(string query)
    {
        try
        {
            string errormessage = "";
            OdbcCommand com = new OdbcCommand(query);
            using(OdbcConnection con = DBConnection.getconnectionString(errormessage))
            {
                con.Open();
                using(OdbcDataAdapter sda = new OdbcDataAdapter())
                {
                    com.Connection = con;
                    sda.SelectCommand = com;
                    using(allocation allocation = new allocation())
                    {
                        sda.Fill(allocation, "DataTable1");
                        return allocation;
                    }
                }

            }
        }
        catch(Exception)
        {

            throw;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {

        string dtfrom;
        string dtto;
        //string datee;

        dtfrom = Convert.ToDateTime(txtfrom.Text).ToString("yyyy-MM-dd");
        dtto = Convert.ToDateTime(txttto.Text).ToString("yyyy-MM-dd");

        string date = "%" + dtto + "%";
        ReportViewer1.ProcessingMode = ProcessingMode.Local;
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/allocationreport.rdlc");
        allocation allocation = GetData("select * from  acceptedallocation WHERE date between'" + dtfrom + "' and '" + dtto + "' OR `date` LIKE '" + date + "'");
        ReportDataSource datasource = new ReportDataSource("DataSet1", allocation.Tables[0]);
        ReportViewer1.LocalReport.DataSources.Clear();
        ReportViewer1.LocalReport.DataSources.Add(datasource);
    }
}