﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Odbc;
public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        txtusername.Focus();
    }


    protected void ShowMessage(string Message, EnumMessages.MessageType type)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
    }

    protected void btnlogin_Click(object sender, EventArgs e)
    {
        loginaccess();
    }

    public void loginaccess()
    {
        try
        {
            string loginquery = DBqueries.loginquery;
            using(OdbcConnection con = DBConnection.getconnectionString(lblerror.Text))
            {
                con.Open();
                using(OdbcCommand com = new OdbcCommand(loginquery, con))
                {
                    com.Parameters.AddWithValue("?username", txtusername.Text.Trim());
                    com.Parameters.AddWithValue("?password", txtpassword.Text.Trim());
                    OdbcDataReader reader = null;
                    OdbcDataAdapter da = new OdbcDataAdapter(com);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    reader = com.ExecuteReader();
                    while(reader.Read())
                    {
                        Globalvariables.masteruser = reader["masteruser"].ToString();
                        Globalvariables.username = txtusername.Text;
                    }

                    if(Globalvariables.masteruser == "YES")
                    {
                        Response.Redirect("mastermenu.aspx");
                    }
                    else
                    {
                        Response.Redirect("menu.aspx");
                    }
                }
            }

        }
        catch(Exception ex)
        {
            //lblerror.ForeColor = System.Drawing.Color.Red;
            //lblerror.Text = "Oops an error has occured";
            ShowMessage("Oops an error has occured.", EnumMessages.MessageType.Error);
        }
    }
}