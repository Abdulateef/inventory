﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="registrationpage.aspx.cs" Inherits="registrationpage"  UnobtrusiveValidationMode="None" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Registration Page</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="Styles/styles.css" rel="stylesheet" />
    <link href="Styles/Pretty-Registration-Form.css" rel="stylesheet" />
    <script src="Scripts/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="Scripts/alertmessages.js"></script>
     <style type="text/css">

        .messagealert {
            width: 30%;
            position: fixed;
             top:0px;
            z-index: 100000;
            padding: 0;
            font-size: 15px;
            margin-right: 50%
        }
    </style>
</head>
<body>
    
     <div class="row register-form">
        <div class="col-md-8 col-md-offset-2">
            <form  id="form1"  runat="server" class ="form-horizontal custom-form">
                <div class="messagealert" id="alert_container">
            </div>
                <p>
                    <asp:Label runat="server" Text="" ID="lblerror"></asp:Label>
                </p>
                <h1>Registration Form</h1>

                <div class="form-group">
                    <div class="col-sm-4 label-column">
                        <label class="control-label" for="repeat-pawssword-input-field">Full Name </label>
                    </div>
                    <div class="col-sm-6 input-column">
                        <asp:TextBox runat="server" ID="txtfullname" CssClass="form-control" placeholder="Enter Full Name" required="true"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4 label-column">
                        <label class="control-label" for="name-input-field">Name</label>
                    </div>
                    <div class="col-sm-6 input-column">
                        <asp:TextBox runat="server" ID="txtname" class="form-control" AutoCompleteType="DisplayName" type="text" placeholder="User Name" required="true"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4 label-column">
                        <label class="control-label" for="email-input-field">Email </label>
                    </div>
                    <div class="col-sm-6 input-column">
                        <asp:TextBox runat="server" ID="txtemail" AutoCompleteType="Email" CssClass="form-control" TextMode="Email" placeholder="Your Email" required="true">
                        </asp:TextBox>
               <%-- <asp:RegularExpressionValidator runat="server" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                ControlToValidate="txtemail" ForeColor="Red" ErrorMessage="Invalid email address." />
                      <%--  <input class="form-control" type="email">--%>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4 label-column">
                        <label class="control-label" for="pawssword-input-field">Password </label>
                    </div>
                    <div class="col-sm-6 input-column">
                        <asp:TextBox runat="server" ID="txtpassword" CssClass="form-control" TextMode="Password"></asp:TextBox>
                       <%-- <input class="form-control" type="password">--%>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4 label-column">
                        <label class="control-label" for="repeat-pawssword-input-field">Repeat Password </label>
                    </div>
                    <div class="col-sm-6 input-column">
                        <asp:TextBox runat="server" TextMode="Password" CssClass="form-control" ID="txtcfmpassword"></asp:TextBox>
                        <asp:CompareValidator ErrorMessage="Passwords do not match." ForeColor="Red" ControlToCompare="txtpassword"
                        ControlToValidate="txtcfmpassword" runat="server" />
                        <%--<input class="form-control" type="password">--%>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4 label-column">
                        <label class="control-label" for="dropdown-input-field">Master User</label>
                    </div>
                    <div class="col-sm-4 input-column">
                        <div class="dropdown">
                            <asp:DropDownList runat="server" ID="drpduserrow" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" >
                                 <asp:ListItem Text="NO" Value="NO" Selected="True"> </asp:ListItem>
                                <asp:ListItem Text="YES" Value="YES" Selected="False"> </asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="checkbox">
                    <label>
                        <asp:CheckBox runat="server" Text="I've read and accept the terms and conditions" />
                    </label>
                </div>
                <asp:Button runat="server" class="btn btn-info" ID="btnregister" Text="Register User" OnClick="btnregister_Click"/>
                <a href="login.aspx" class="btn btn-success">Log In</a>
            </form>
        </div>
    </div>
    <script src="Scripts/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
